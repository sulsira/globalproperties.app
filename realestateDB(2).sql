-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 12, 2014 at 11:13 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.6.0-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `realestateDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `Addr_AddressID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Addr_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_AddressStreet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_POBox` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_District` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_CareOf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Addr_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Addr_AddressID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=111 ;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`Addr_AddressID`, `Addr_EntityID`, `Addr_EntityType`, `Addr_AddressStreet`, `Addr_POBox`, `Addr_Town`, `Addr_Region`, `Addr_District`, `Addr_Country`, `Addr_CareOf`, `Addr_Deleted`, `created_at`, `updated_at`) VALUES
(1, '2', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:03:26', '2014-08-22 01:03:26'),
(2, '3', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:06:45', '2014-08-22 01:06:45'),
(3, '4', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(4, '9', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:46:37', '2014-08-22 01:46:37'),
(5, '10', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:48:55', '2014-08-22 01:48:55'),
(6, '11', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:57:52', '2014-08-22 01:57:52'),
(7, '12', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:58:20', '2014-08-22 01:58:20'),
(8, '13', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(9, '14', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(10, '15', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(11, '16', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(12, '17', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(13, '18', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(14, '19', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(15, '20', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(16, '21', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(17, '22', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(18, '23', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(19, '24', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(20, '25', 'Person', 'asdfasdfa', NULL, 'asdfasdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(21, '26', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(22, '27', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:39:11', '2014-08-22 15:39:11'),
(23, '28', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:40:00', '2014-08-22 15:40:00'),
(24, '29', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(25, '30', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(26, '31', 'Person', 'street', NULL, 'two', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(27, '32', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(28, '33', 'Person', 'dr. cessay', NULL, 'kololi', NULL, 'Serre Kunda West', NULL, NULL, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(29, '4', 'Landlord', 'asdfa adsafdsa asd', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:16:44', '2014-09-06 03:16:44'),
(30, '5', 'Landlord', 'adsagsfga adsfa adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:18:21', '2014-09-06 03:18:21'),
(31, '6', 'Landlord', '2435252 asdfa vafda', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:20:59', '2014-09-06 03:20:59'),
(32, '7', 'Landlord', '2234q', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:26:05', '2014-09-06 03:26:05'),
(33, '8', 'Landlord', '23dfafa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:29:56', '2014-09-06 03:29:56'),
(34, '9', 'Landlord', '23432afaf', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:32:03', '2014-09-06 03:32:03'),
(35, '10', 'Landlord', 'dfasfa asdfaf adsfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:33:06', '2014-09-06 03:33:06'),
(36, '11', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:34:12', '2014-09-06 03:34:12'),
(37, '12', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:35:35', '2014-09-06 03:35:35'),
(38, '13', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:37:26', '2014-09-06 03:37:26'),
(39, '14', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:38:21', '2014-09-06 03:38:21'),
(40, '15', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:38:42', '2014-09-06 03:38:42'),
(41, '16', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:39:03', '2014-09-06 03:39:03'),
(42, '17', 'Landlord', '23afdsfasdfsa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:39:34', '2014-09-06 03:39:34'),
(43, '18', 'Landlord', ' adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 03:43:40', '2014-09-06 03:43:40'),
(44, '19', 'Landlord', ' adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 04:00:51', '2014-09-06 04:00:51'),
(45, '20', 'Landlord', ' adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 04:01:52', '2014-09-06 04:01:52'),
(46, '21', 'Landlord', ' adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 04:04:29', '2014-09-06 04:04:29'),
(47, '22', 'Landlord', ' adfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 04:05:50', '2014-09-06 04:05:50'),
(48, '34', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 05:10:39', '2014-09-06 05:10:39'),
(49, '35', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 05:11:23', '2014-09-06 05:11:23'),
(50, '36', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 05:14:49', '2014-09-06 05:14:49'),
(51, '37', 'Person', 'asdfaf', NULL, 'sdfafsa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:14:20', '2014-09-06 06:14:20'),
(52, '38', 'Person', 'asdfa', NULL, 'asdfafa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:15:51', '2014-09-06 06:15:51'),
(53, '39', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:17:23', '2014-09-06 06:17:23'),
(54, '40', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:19:28', '2014-09-06 06:19:28'),
(55, '41', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:20:43', '2014-09-06 06:20:43'),
(56, '2', 'Landlord', 'there is a somethi', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 06:21:44', '2014-09-06 06:21:44'),
(57, '3', 'Landlord', '23423afdsfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 06:23:54', '2014-09-06 06:23:54'),
(58, '42', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:28:44', '2014-09-06 06:28:44'),
(59, '43', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:31:53', '2014-09-06 06:31:53'),
(60, '44', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:32:28', '2014-09-06 06:32:28'),
(61, '45', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 06:33:07', '2014-09-06 06:33:07'),
(62, '46', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 07:03:48', '2014-09-06 07:03:48'),
(63, '47', 'Person', 'steet', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 07:21:16', '2014-09-06 07:21:16'),
(64, '48', 'Person', 'asdfasdfa', NULL, 'asdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 07:30:11', '2014-09-06 07:30:11'),
(65, '5', 'Landlord', 'asdfafa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 07:52:58', '2014-09-06 07:52:58'),
(66, '6', 'Landlord', '234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 07:53:40', '2014-09-06 07:53:40'),
(67, '49', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:02:57', '2014-09-06 15:02:57'),
(68, '50', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:17:40', '2014-09-06 15:17:40'),
(69, '51', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:45:30', '2014-09-06 15:45:30'),
(70, '52', 'Person', '', NULL, '2324', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:47:34', '2014-09-06 15:47:34'),
(71, '53', 'Person', '', NULL, '2324', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:48:16', '2014-09-06 15:48:16'),
(72, '54', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 15:49:28', '2014-09-06 15:49:28'),
(73, '55', 'Person', 'kololi village', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:16:13', '2014-09-06 16:16:13'),
(74, '56', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:18:54', '2014-09-06 16:18:54'),
(75, '57', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:19:55', '2014-09-06 16:19:55'),
(76, '58', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:21:21', '2014-09-06 16:21:21'),
(77, '59', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:30:48', '2014-09-06 16:30:48'),
(78, '60', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:32:05', '2014-09-06 16:32:05'),
(79, '61', 'Person', 'sdfasdfa', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:33:20', '2014-09-06 16:33:20'),
(80, '62', 'Person', 'king', NULL, 'lamen', NULL, 'Kombo East', NULL, NULL, 0, '2014-09-06 18:44:23', '2014-09-06 18:44:23'),
(81, '63', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 18:48:07', '2014-09-06 18:48:07'),
(82, '64', 'Person', '', NULL, 'asdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 19:03:13', '2014-09-06 19:03:13'),
(83, '65', 'Person', '', NULL, 'asdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 19:04:33', '2014-09-06 19:04:33'),
(84, '66', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 19:05:05', '2014-09-06 19:05:05'),
(85, '67', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 19:05:34', '2014-09-06 19:05:34'),
(86, '68', 'Person', '', NULL, '', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-06 20:58:09', '2014-09-06 20:58:09'),
(87, '69', 'Person', 'kololi', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 22:05:44', '2014-09-06 22:05:44'),
(88, '70', 'Person', 'asdaf', NULL, 'twsdf', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-07 03:45:54', '2014-09-07 03:45:54'),
(89, '71', 'Person', '', NULL, 'afsdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-07 03:46:34', '2014-09-07 03:46:34'),
(90, '72', 'Person', '', NULL, 'afsdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-07 03:48:38', '2014-09-07 03:48:38'),
(91, '73', 'Person', '', NULL, 'afsdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(92, '74', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-09 22:50:59', '2014-09-09 22:50:59'),
(93, '75', 'Person', 'asdfa', NULL, 'dfasda', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(94, '76', 'Person', 'asdfa', NULL, 'sdfasdfa', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-09 23:06:07', '2014-09-09 23:06:07'),
(95, '77', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(96, '78', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(97, '79', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(98, '80', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(99, '81', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(100, '82', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(101, '83', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(102, '84', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(103, '85', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(104, '86', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(105, '87', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(106, '88', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(107, '89', 'Person', 'psdrammeh@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-10 18:48:36', '2014-09-10 18:48:36'),
(108, '90', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(109, '91', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-12 06:55:48', '2014-09-12 06:55:48'),
(110, '92', 'Person', 'street', NULL, 'town', NULL, 'Banjul South', NULL, NULL, 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE IF NOT EXISTS `agents` (
  `agen_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agen_persID` int(11) NOT NULL DEFAULT '0',
  `agen_contID` int(11) NOT NULL DEFAULT '0',
  `agen_addrID` int(11) NOT NULL DEFAULT '0',
  `agen_prospID` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`agen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`agen_id`, `agen_persID`, `agen_contID`, `agen_addrID`, `agen_prospID`, `created_at`, `updated_at`) VALUES
(1, 24, 35, 19, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(2, 25, 38, 20, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `agent_prospectives`
--

CREATE TABLE IF NOT EXISTS `agent_prospectives` (
  `apro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `apro_persID` int(11) NOT NULL DEFAULT '0',
  `apro_agenID` int(11) NOT NULL DEFAULT '0',
  `apro_addrID` int(11) NOT NULL DEFAULT '0',
  `apro_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apro_deleted` int(11) NOT NULL DEFAULT '0',
  `apro_visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`apro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `calender_table`
--

CREATE TABLE IF NOT EXISTS `calender_table` (
  `caln_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caln_rentID` int(11) NOT NULL DEFAULT '0',
  `caln_squence` int(11) NOT NULL DEFAULT '0',
  `caln_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `caln_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`caln_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `compounds`
--

CREATE TABLE IF NOT EXISTS `compounds` (
  `comp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comp_landLordID` int(11) NOT NULL DEFAULT '0',
  `comp_numberOfHouses` int(11) NOT NULL DEFAULT '0',
  `comp_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comp_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comp_indentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`comp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `compounds`
--

INSERT INTO `compounds` (`comp_id`, `comp_landLordID`, `comp_numberOfHouses`, `comp_size`, `comp_remarks`, `comp_indentifier`, `deleted`, `created_at`, `updated_at`, `location`) VALUES
(1, 0, 243523, 'asdf', 'asdfsafa', 'sdfasdfa', 0, '2014-09-06 20:07:42', '2014-09-06 20:07:42', 'asdf'),
(2, 0, 243523, 'asdf', 'asdfsafa', 'sdfasdfa', 0, '2014-09-06 20:08:05', '2014-09-06 20:08:05', 'asdf'),
(3, 0, 243523, 'asdf', 'asdfsafa', 'sdfasdfa', 0, '2014-09-06 20:31:29', '2014-09-06 20:31:29', 'asdf'),
(4, 0, 25, '260 X 250', 'asdfasda', 'asdkl;fja;f', 0, '2014-09-06 20:56:23', '2014-09-06 20:56:23', 'kololi'),
(5, 2, 28, NULL, NULL, NULL, 0, '2014-09-06 22:05:44', '2014-09-06 22:05:44', 'kololi'),
(6, 0, 32, 'asdfa', 'asdfasdfa', 'asdfa', 0, '2014-09-06 22:37:24', '2014-09-06 22:37:24', 'asdfasdfa'),
(7, 0, 23432, 'dfa', 'asdfa', 'sdf', 0, '2014-09-06 22:38:16', '2014-09-06 22:38:16', 'sdfa'),
(8, 3, 2, NULL, NULL, NULL, 0, '2014-09-10 18:48:36', '2014-09-10 18:48:36', 'Brusubi');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Cont_ContactInfoID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_EntityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_ContactType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Cont_Deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=193 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `Cont_ContactInfoID`, `Cont_EntityID`, `Cont_EntityType`, `Cont_Contact`, `Cont_ContactType`, `Cont_Deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, '4', 'Person', '7052177', 'phones', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(2, NULL, '4', 'Person', 'ema@email', 'email', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(3, NULL, '4', 'Person', '5465846', 'telephone', 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(4, NULL, '13', 'Person', '705214', 'phones', 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(5, NULL, '14', 'Person', '705214', 'phones', 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(6, NULL, '15', 'Person', '705214', 'phones', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(7, NULL, '15', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(8, NULL, '15', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(9, NULL, '16', 'Person', '705214', 'phones', 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(10, NULL, '16', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(11, NULL, '16', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:11:15', '2014-08-22 02:11:15'),
(12, NULL, '17', 'Person', '705214', 'phones', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(13, NULL, '17', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(14, NULL, '17', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(15, NULL, '18', 'Person', '705214', 'phones', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(16, NULL, '18', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(17, NULL, '18', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(18, NULL, '19', 'Person', '705214', 'phones', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(19, NULL, '19', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(20, NULL, '19', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(21, NULL, '20', 'Person', '705214', 'phones', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(22, NULL, '20', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(23, NULL, '20', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(24, NULL, '21', 'Person', '705214', 'phones', 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(25, NULL, '21', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:22:46', '2014-08-22 02:22:46'),
(26, NULL, '21', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:22:46', '2014-08-22 02:22:46'),
(27, NULL, '22', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(28, NULL, '22', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(29, NULL, '22', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(30, NULL, '23', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(31, NULL, '23', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(32, NULL, '23', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(33, NULL, '24', 'Person', '705214', 'phones', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(34, NULL, '24', 'Person', 'emait@ema.com', 'email', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(35, NULL, '24', 'Person', '546841687', 'telephone', 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(36, NULL, '25', 'Person', '565464', 'phones', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(37, NULL, '25', 'Person', 'sdfgsdgsgs', 'email', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(38, NULL, '25', 'Person', '34535345453', 'telephone', 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(39, NULL, '26', 'Person', '676785676', 'phones', 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(40, NULL, '30', 'Person', '234234234', 'phones', 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(41, NULL, '30', 'Person', '23423423', 'telephone', 0, '2014-08-22 16:37:08', '2014-08-22 16:37:08'),
(42, NULL, '31', 'Person', '7052217', 'phones', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(43, NULL, '31', 'Person', 'sulsira@something.com', 'email', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(44, NULL, '31', 'Person', '765464546', 'telephone', 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(45, NULL, '32', 'Person', '7052217', 'phones', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(46, NULL, '32', 'Person', 'sulsira@hotm.com', 'email', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(47, NULL, '32', 'Person', '7654656', 'telephone', 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(48, NULL, '33', 'Person', '7052217', 'phones', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(49, NULL, '33', 'Person', 'somethin@email.com', 'email', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(50, NULL, '33', 'Person', '6677578', 'telephone', 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(51, NULL, '3', 'Landlord', 'fasdfa252352', 'Phone', 0, '2014-09-06 03:11:56', '2014-09-06 03:11:56'),
(52, NULL, '4', 'Landlord', '23423524', 'Phone', 0, '2014-09-06 03:16:44', '2014-09-06 03:16:44'),
(53, NULL, '5', 'Landlord', '424242,4242,3453', 'Phone', 0, '2014-09-06 03:18:21', '2014-09-06 03:18:21'),
(54, NULL, '6', 'Landlord', '234235342', 'Phone', 0, '2014-09-06 03:20:59', '2014-09-06 03:20:59'),
(55, NULL, '7', 'Landlord', 'asda', 'Phone', 0, '2014-09-06 03:26:05', '2014-09-06 03:26:05'),
(56, NULL, '8', 'Landlord', '2323432', 'Phone', 0, '2014-09-06 03:29:56', '2014-09-06 03:29:56'),
(57, NULL, '9', 'Landlord', '234322', 'Phone', 0, '2014-09-06 03:32:03', '2014-09-06 03:32:03'),
(58, NULL, '10', 'Landlord', '44535243', 'Phone', 0, '2014-09-06 03:33:06', '2014-09-06 03:33:06'),
(59, NULL, '11', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:34:12', '2014-09-06 03:34:12'),
(60, NULL, '12', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:35:35', '2014-09-06 03:35:35'),
(61, NULL, '13', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:37:26', '2014-09-06 03:37:26'),
(62, NULL, '14', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:38:21', '2014-09-06 03:38:21'),
(63, NULL, '15', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:38:42', '2014-09-06 03:38:42'),
(64, NULL, '16', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:39:03', '2014-09-06 03:39:03'),
(65, NULL, '17', 'Landlord', '23423242523', 'Phone', 0, '2014-09-06 03:39:34', '2014-09-06 03:39:34'),
(66, NULL, '18', 'Landlord', '24235', 'Phone', 0, '2014-09-06 03:43:40', '2014-09-06 03:43:40'),
(67, NULL, '19', 'Landlord', '24235', 'Phone', 0, '2014-09-06 04:00:51', '2014-09-06 04:00:51'),
(68, NULL, '20', 'Landlord', '24235', 'Phone', 0, '2014-09-06 04:01:52', '2014-09-06 04:01:52'),
(69, NULL, '21', 'Landlord', '24235', 'Phone', 0, '2014-09-06 04:04:29', '2014-09-06 04:04:29'),
(70, NULL, '22', 'Landlord', '24235', 'Phone', 0, '2014-09-06 04:05:50', '2014-09-06 04:05:50'),
(71, NULL, '36', 'Person', '124131312', 'phones', 0, '2014-09-06 05:14:49', '2014-09-06 05:14:49'),
(72, NULL, '36', 'Person', '23afa232', 'email', 0, '2014-09-06 05:14:49', '2014-09-06 05:14:49'),
(73, NULL, '36', 'Person', '23423234523', 'telephone', 0, '2014-09-06 05:14:49', '2014-09-06 05:14:49'),
(74, NULL, '37', 'Person', 'asdfas', 'phones', 0, '2014-09-06 06:14:20', '2014-09-06 06:14:20'),
(75, NULL, '37', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:14:20', '2014-09-06 06:14:20'),
(76, NULL, '37', 'Person', 'asdfafadsf', 'telephone', 0, '2014-09-06 06:14:20', '2014-09-06 06:14:20'),
(77, NULL, '38', 'Person', 'adsfa', 'phones', 0, '2014-09-06 06:15:51', '2014-09-06 06:15:51'),
(78, NULL, '38', 'Person', 'adsfasdfa', 'email', 0, '2014-09-06 06:15:51', '2014-09-06 06:15:51'),
(79, NULL, '38', 'Person', 'adsfa', 'telephone', 0, '2014-09-06 06:15:51', '2014-09-06 06:15:51'),
(80, NULL, '39', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:17:23', '2014-09-06 06:17:23'),
(81, NULL, '39', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:17:23', '2014-09-06 06:17:23'),
(82, NULL, '39', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:17:23', '2014-09-06 06:17:23'),
(83, NULL, '40', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:19:28', '2014-09-06 06:19:28'),
(84, NULL, '40', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:19:28', '2014-09-06 06:19:28'),
(85, NULL, '40', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:19:28', '2014-09-06 06:19:28'),
(86, NULL, '41', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:20:43', '2014-09-06 06:20:43'),
(87, NULL, '41', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:20:43', '2014-09-06 06:20:43'),
(88, NULL, '41', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:20:43', '2014-09-06 06:20:43'),
(89, NULL, '2', 'Landlord', '7021457', 'Phone', 0, '2014-09-06 06:21:44', '2014-09-06 06:21:44'),
(90, NULL, '3', 'Landlord', '2335r34343', 'Phone', 0, '2014-09-06 06:23:54', '2014-09-06 06:23:54'),
(91, NULL, '42', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:28:44', '2014-09-06 06:28:44'),
(92, NULL, '42', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:28:44', '2014-09-06 06:28:44'),
(93, NULL, '42', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:28:44', '2014-09-06 06:28:44'),
(94, NULL, '43', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:31:53', '2014-09-06 06:31:53'),
(95, NULL, '43', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:31:53', '2014-09-06 06:31:53'),
(96, NULL, '43', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:31:53', '2014-09-06 06:31:53'),
(97, NULL, '44', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:32:28', '2014-09-06 06:32:28'),
(98, NULL, '44', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:32:28', '2014-09-06 06:32:28'),
(99, NULL, '44', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:32:28', '2014-09-06 06:32:28'),
(100, NULL, '45', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 06:33:07', '2014-09-06 06:33:07'),
(101, NULL, '45', 'Person', 'asdfa', 'email', 0, '2014-09-06 06:33:07', '2014-09-06 06:33:07'),
(102, NULL, '45', 'Person', 'asd', 'telephone', 0, '2014-09-06 06:33:07', '2014-09-06 06:33:07'),
(103, NULL, '46', 'Person', 'asdfasdfa', 'phones', 0, '2014-09-06 07:03:48', '2014-09-06 07:03:48'),
(104, NULL, '46', 'Person', 'asdfa', 'email', 0, '2014-09-06 07:03:48', '2014-09-06 07:03:48'),
(105, NULL, '46', 'Person', 'asd', 'telephone', 0, '2014-09-06 07:03:48', '2014-09-06 07:03:48'),
(106, NULL, '47', 'Person', '7055217', 'phones', 0, '2014-09-06 07:21:16', '2014-09-06 07:21:16'),
(107, NULL, '47', 'Person', 'ssuls@homc.om', 'email', 0, '2014-09-06 07:21:16', '2014-09-06 07:21:16'),
(108, NULL, '47', 'Person', '6546546', 'telephone', 0, '2014-09-06 07:21:16', '2014-09-06 07:21:16'),
(109, NULL, '48', 'Person', '7052217', 'phones', 0, '2014-09-06 07:30:11', '2014-09-06 07:30:11'),
(110, NULL, '5', 'Landlord', '23525', 'Phone', 0, '2014-09-06 07:52:58', '2014-09-06 07:52:58'),
(111, NULL, '6', 'Landlord', '3423423', 'Phone', 0, '2014-09-06 07:53:40', '2014-09-06 07:53:40'),
(112, NULL, '54', 'Person', '23255234', 'phones', 0, '2014-09-06 15:49:28', '2014-09-06 15:49:28'),
(113, NULL, '54', 'Person', 'asdfa', 'email', 0, '2014-09-06 15:49:28', '2014-09-06 15:49:28'),
(114, NULL, '54', 'Person', '353523452', 'telephone', 0, '2014-09-06 15:49:28', '2014-09-06 15:49:28'),
(115, NULL, '55', 'Person', '2516483', 'Phone', 0, '2014-09-06 16:16:13', '2014-09-06 16:16:13'),
(116, NULL, '56', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:18:54', '2014-09-06 16:18:54'),
(117, NULL, '57', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:19:55', '2014-09-06 16:19:55'),
(118, NULL, '58', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:21:21', '2014-09-06 16:21:21'),
(119, NULL, '59', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:30:48', '2014-09-06 16:30:48'),
(120, NULL, '60', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:32:05', '2014-09-06 16:32:05'),
(121, NULL, '61', 'Person', '452252342', 'Phone', 0, '2014-09-06 16:33:20', '2014-09-06 16:33:20'),
(122, NULL, '62', 'Person', '7516121652, 146511,25541', 'phones', 0, '2014-09-06 18:44:23', '2014-09-06 18:44:23'),
(123, NULL, '62', 'Person', 'some@som.com', 'email', 0, '2014-09-06 18:44:23', '2014-09-06 18:44:23'),
(124, NULL, '62', 'Person', '65464645', 'telephone', 0, '2014-09-06 18:44:23', '2014-09-06 18:44:23'),
(125, NULL, '64', 'Person', 'asdf', 'phones', 0, '2014-09-06 19:03:13', '2014-09-06 19:03:13'),
(126, NULL, '65', 'Person', 'asdf', 'phones', 0, '2014-09-06 19:04:33', '2014-09-06 19:04:33'),
(127, NULL, '66', 'Person', '23423452123', 'phones', 0, '2014-09-06 19:05:05', '2014-09-06 19:05:05'),
(128, NULL, '66', 'Person', '423423423', 'telephone', 0, '2014-09-06 19:05:05', '2014-09-06 19:05:05'),
(129, NULL, '69', 'Person', '75021741', 'Phone', 0, '2014-09-06 22:05:44', '2014-09-06 22:05:44'),
(130, NULL, '70', 'Person', 'adfa', 'phones', 0, '2014-09-07 03:45:54', '2014-09-07 03:45:54'),
(131, NULL, '70', 'Person', 'dsfa', 'email', 0, '2014-09-07 03:45:54', '2014-09-07 03:45:54'),
(132, NULL, '70', 'Person', 'asdf', 'telephone', 0, '2014-09-07 03:45:54', '2014-09-07 03:45:54'),
(133, NULL, '71', 'Person', 'asdfads', 'phones', 0, '2014-09-07 03:46:34', '2014-09-07 03:46:34'),
(134, NULL, '71', 'Person', 'sadf', 'email', 0, '2014-09-07 03:46:34', '2014-09-07 03:46:34'),
(135, NULL, '71', 'Person', 'asdf', 'telephone', 0, '2014-09-07 03:46:34', '2014-09-07 03:46:34'),
(136, NULL, '72', 'Person', 'asdfads', 'phones', 0, '2014-09-07 03:48:38', '2014-09-07 03:48:38'),
(137, NULL, '72', 'Person', 'sadf', 'email', 0, '2014-09-07 03:48:38', '2014-09-07 03:48:38'),
(138, NULL, '72', 'Person', 'asdf', 'telephone', 0, '2014-09-07 03:48:38', '2014-09-07 03:48:38'),
(139, NULL, '73', 'Person', 'asdfads', 'phones', 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(140, NULL, '73', 'Person', 'sadf', 'email', 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(141, NULL, '73', 'Person', 'asdf', 'telephone', 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(142, NULL, '74', 'Person', '702254', 'phones', 0, '2014-09-09 22:50:59', '2014-09-09 22:50:59'),
(143, NULL, '74', 'Person', 'sdfasdfa', 'email', 0, '2014-09-09 22:50:59', '2014-09-09 22:50:59'),
(144, NULL, '75', 'Person', 'asdfas', 'phones', 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(145, NULL, '75', 'Person', 'sdfa', 'email', 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(146, NULL, '75', 'Person', 'asdfa', 'telephone', 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(147, NULL, '77', 'Person', '2343242', 'phones', 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(148, NULL, '77', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(149, NULL, '77', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(150, NULL, '78', 'Person', '2343242', 'phones', 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(151, NULL, '78', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(152, NULL, '78', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(153, NULL, '79', 'Person', '2343242', 'phones', 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(154, NULL, '79', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(155, NULL, '79', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(156, NULL, '80', 'Person', '2343242', 'phones', 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(157, NULL, '80', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(158, NULL, '80', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(159, NULL, '81', 'Person', '2343242', 'phones', 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(160, NULL, '81', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(161, NULL, '81', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(162, NULL, '82', 'Person', '2343242', 'phones', 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(163, NULL, '82', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(164, NULL, '82', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(165, NULL, '83', 'Person', '2343242', 'phones', 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(166, NULL, '83', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(167, NULL, '83', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(168, NULL, '84', 'Person', '2343242', 'phones', 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(169, NULL, '84', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(170, NULL, '84', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(171, NULL, '85', 'Person', '2343242', 'phones', 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(172, NULL, '85', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(173, NULL, '85', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(174, NULL, '86', 'Person', '2343242', 'phones', 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(175, NULL, '86', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(176, NULL, '86', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(177, NULL, '87', 'Person', '2343242', 'phones', 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(178, NULL, '87', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(179, NULL, '87', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(180, NULL, '88', 'Person', '2343242', 'phones', 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(181, NULL, '88', 'Person', 'sulsira@hotmil.co', 'email', 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(182, NULL, '88', 'Person', '2342342342', 'telephone', 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(183, NULL, '89', 'Person', '+2207251229', 'Phone', 0, '2014-09-10 18:48:36', '2014-09-10 18:48:36'),
(184, NULL, '90', 'Person', '654645', 'phones', 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(185, NULL, '90', 'Person', 'sulsira@hotmail.com', 'email', 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(186, NULL, '90', 'Person', '65664564', 'telephone', 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(187, NULL, '91', 'Person', '7052217', 'phones', 0, '2014-09-12 06:55:48', '2014-09-12 06:55:48'),
(188, NULL, '91', 'Person', 'sulsira@hotmcail', 'email', 0, '2014-09-12 06:55:48', '2014-09-12 06:55:48'),
(189, NULL, '91', 'Person', '6546546', 'telephone', 0, '2014-09-12 06:55:48', '2014-09-12 06:55:48'),
(190, NULL, '92', 'Person', '7052217', 'phones', 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06'),
(191, NULL, '92', 'Person', 'sulsira@hotmcail', 'email', 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06'),
(192, NULL, '92', 'Person', '6546546', 'telephone', 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `cust_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cust_plotID` int(11) NOT NULL DEFAULT '0',
  `cust_downpayment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_partnerID` int(11) NOT NULL DEFAULT '0',
  `cust_personID` int(11) NOT NULL DEFAULT '0',
  `cust_ruleID` int(11) NOT NULL DEFAULT '0',
  `cust_transID` int(11) NOT NULL DEFAULT '0',
  `cust_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cust_lanID` int(11) NOT NULL DEFAULT '0',
  `cust_agenID` int(11) NOT NULL DEFAULT '0',
  `cust_payID` int(11) NOT NULL DEFAULT '0',
  `cust_deleted` int(11) NOT NULL DEFAULT '0',
  `cust_visible` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cust_id`, `cust_plotID`, `cust_downpayment`, `cust_partnerID`, `cust_personID`, `cust_ruleID`, `cust_transID`, `cust_status`, `cust_lanID`, `cust_agenID`, `cust_payID`, `cust_deleted`, `cust_visible`, `created_at`, `updated_at`) VALUES
(1, 2, 'adfa', 0, 29, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(2, 1, '200.3', 0, 30, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-22 16:37:08', '2014-08-22 16:37:08'),
(3, 3, '20000', 0, 31, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(4, 3, '2500', 0, 32, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(5, 5, '280000', 0, 33, 0, 0, NULL, 0, 0, 0, 0, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `exp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exp_houseID` int(11) NOT NULL DEFAULT '0',
  `exp_compoundID` int(11) NOT NULL DEFAULT '0',
  `exp_tenanceID` int(11) NOT NULL DEFAULT '0',
  `exp_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `exp_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`exp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE IF NOT EXISTS `houses` (
  `hous_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hous_compoundID` int(11) NOT NULL DEFAULT '0',
  `hous_tenantID` int(11) NOT NULL DEFAULT '0',
  `hous_advance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `hous_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `hous_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_paymentStype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hous_status` tinyint(1) NOT NULL DEFAULT '0',
  `hous_numberOfrooms` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`hous_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`hous_id`, `hous_compoundID`, `hous_tenantID`, `hous_advance`, `hous_number`, `hous_price`, `hous_description`, `hous_paymentStype`, `hous_availability`, `hous_status`, `hous_numberOfrooms`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 4, 0, 2233.00, 'dfasdfasdfa', 23432.00, 'asdfasdfa asdfa', NULL, NULL, 0, 23, 0, '2014-09-07 00:41:52', '2014-09-07 00:41:52'),
(2, 0, 0, 23432.00, 'asdfadsf', 4524.00, 'asdfasdfadsfasdf', NULL, NULL, 0, 23, 0, '2014-09-07 02:14:29', '2014-09-07 02:14:29');

-- --------------------------------------------------------

--
-- Table structure for table `landlords`
--

CREATE TABLE IF NOT EXISTS `landlords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ll_personid` int(11) NOT NULL DEFAULT '0',
  `ll_fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `landlords`
--

INSERT INTO `landlords` (`id`, `ll_personid`, `ll_fullname`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 68, 'asdfasdfa asdfadf adsfadsfa', 0, '2014-09-06 20:58:09', '2014-09-06 20:58:09'),
(2, 69, 'mama dou jallow', 0, '2014-09-06 22:05:44', '2014-09-06 22:05:44'),
(3, 89, 'PaSara Drammeh', 0, '2014-09-10 18:48:36', '2014-09-10 18:48:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_09_06_052512_add_lrds_personID_landlords_table', 1),
('2014_09_06_150910_drop_landlords_table', 2),
('2014_09_06_191314_add_compounds_table', 2),
('2014_09_06_205054_create_landlords_tables', 3),
('2014_09_06_234504_add_houses_table', 4),
('2014_09_07_141550_add_tenants_table', 5),
('2014_09_07_234255_add_users_roles_table', 6),
('2014_09_09_234730_create_rent_payments_table', 7),
('2014_09_12_054624_add_rents_table', 8),
('2014_09_12_081013_add_rent_payments_table', 9),
('2014_09_12_082443_add_field_rents_table', 10),
('2014_09_12_083409_add_rent_balance_rents_table', 11),
('2014_09_12_092328_add_to_rent_payments_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entityID` int(11) NOT NULL DEFAULT '0',
  `entityType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entityTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `par_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `par_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_relationship` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_payType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `par_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`par_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `paym_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paym_custID` int(11) NOT NULL DEFAULT '0',
  `paym_paidAmount` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_plotID` int(11) NOT NULL DEFAULT '0',
  `paym_balance` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_currentBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `paym_transDate` date DEFAULT NULL,
  `paym_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `paym_userID` int(11) NOT NULL DEFAULT '0',
  `paym_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`paym_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`paym_id`, `paym_custID`, `paym_paidAmount`, `paym_plotID`, `paym_balance`, `paym_currentBal`, `paym_transDate`, `paym_deleted`, `paym_userID`, `paym_code`, `created_at`, `updated_at`) VALUES
(1, 1, 0.000, 2, 0.000, 0.000, '0000-00-00', 0, 2, NULL, '2014-08-24 20:44:59', '2014-08-24 20:44:59'),
(2, 1, 0.000, 2, 0.000, 0.000, '0000-00-00', 0, 2, NULL, '2014-08-24 20:45:18', '2014-08-24 20:45:18'),
(3, 1, 0.000, 2, 0.000, 0.000, '0000-00-00', 0, 2, NULL, '2014-08-24 20:46:55', '2014-08-24 20:46:55'),
(4, 2, 0.000, 1, 0.000, 0.000, '0000-00-00', 0, 2, NULL, '2014-08-29 20:46:39', '2014-08-29 20:46:39'),
(5, 1, 23447.000, 2, 0.000, 0.000, '2014-08-20', 0, 2, NULL, '2014-08-30 01:19:12', '2014-08-30 01:19:12'),
(6, 5, 1000.000, 5, 0.000, 280000.000, '2014-08-31', 0, 2, NULL, '2014-08-30 02:09:58', '2014-08-30 02:09:58'),
(7, 5, 20.455, 5, 0.000, 280000.000, '2014-08-11', 0, 2, NULL, '2014-08-30 21:20:38', '2014-08-30 21:20:38');

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pers_indentifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_givenID` int(11) NOT NULL DEFAULT '0',
  `pers_fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_DOB` date DEFAULT NULL,
  `pers_gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_nationality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_ethnicity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_standing` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_NIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pers_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=93 ;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `pers_indentifier`, `pers_givenID`, `pers_fname`, `pers_mname`, `pers_lname`, `pers_type`, `pers_DOB`, `pers_gender`, `pers_nationality`, `pers_ethnicity`, `pers_standing`, `pers_NIN`, `pers_deleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, 'asdfasdfa', NULL, 'asdfadsaf', 'Agent', '2014-08-19', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 00:55:43', '2014-08-22 00:55:43'),
(2, NULL, 0, 'asdfasdfa', NULL, 'asdfadsaf', 'Agent', '2014-08-19', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:03:26', '2014-08-22 01:03:26'),
(3, NULL, 0, 'first', 'middle', 'jallow', 'Agent', '2014-08-26', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:06:45', '2014-08-22 01:06:45'),
(4, NULL, 0, 'first', 'middle', 'jallow', 'Agent', '2014-08-26', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:08:02', '2014-08-22 01:08:02'),
(5, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:29:50', '2014-08-22 01:29:50'),
(6, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:30:47', '2014-08-22 01:30:47'),
(7, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:32:17', '2014-08-22 01:32:17'),
(8, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:43:03', '2014-08-22 01:43:03'),
(9, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:46:37', '2014-08-22 01:46:37'),
(10, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:48:55', '2014-08-22 01:48:55'),
(11, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:57:52', '2014-08-22 01:57:52'),
(12, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:58:20', '2014-08-22 01:58:20'),
(13, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 01:58:49', '2014-08-22 01:58:49'),
(14, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:09:47', '2014-08-22 02:09:47'),
(15, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:10:52', '2014-08-22 02:10:52'),
(16, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:11:14', '2014-08-22 02:11:14'),
(17, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:11:59', '2014-08-22 02:11:59'),
(18, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:13:53', '2014-08-22 02:13:53'),
(19, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:19:51', '2014-08-22 02:19:51'),
(20, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:20:51', '2014-08-22 02:20:51'),
(21, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:22:45', '2014-08-22 02:22:45'),
(22, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:05', '2014-08-22 02:23:05'),
(23, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:35', '2014-08-22 02:23:35'),
(24, NULL, 0, 'first', 'middle', 'last', 'Agent', '2014-08-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:23:45', '2014-08-22 02:23:45'),
(25, NULL, 0, 'dsfadsfa', 'adfa', 'asdfasf', 'Agent', '2014-08-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:38:02', '2014-08-22 02:38:02'),
(26, NULL, 0, 'ebri', 'so', 'fasdl;fj', 'Staff', '2014-09-03', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 02:46:59', '2014-08-22 02:46:59'),
(27, NULL, 0, 'adsfas', NULL, 'asdfa', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:39:11', '2014-08-22 15:39:11'),
(28, NULL, 0, 'mamadou', NULL, 'jallow', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:40:00', '2014-08-22 15:40:00'),
(29, NULL, 0, 'mamadou', 's', 'jallow', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 15:40:44', '2014-08-22 15:40:44'),
(30, NULL, 0, 'ous', NULL, 'jallow', 'Customer', '2014-08-13', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-22 16:37:07', '2014-08-22 16:37:07'),
(31, NULL, 0, 'mamadou ', 's', 'jallow', 'Customer', '2014-08-27', 'female', 'Argentina', 'Sarahule', NULL, NULL, 0, '2014-08-24 01:23:57', '2014-08-24 01:23:57'),
(32, NULL, 0, 'somethong', 'els', 'something', 'Customer', '2014-08-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-30 01:50:22', '2014-08-30 01:50:22'),
(33, NULL, 0, 'theydont', 's', 'give', 'Customer', '2014-08-31', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-08-30 02:08:27', '2014-08-30 02:08:27'),
(34, NULL, 0, 'sdfasd', 'asdfa', 'asasdfasd', 'Customer', '2014-09-20', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 05:10:39', '2014-09-06 05:10:39'),
(35, NULL, 0, 'asfa', 'asd', 'adsfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 05:11:22', '2014-09-06 05:11:22'),
(36, NULL, 0, 'sdfasdf', 'as', 'asdfa', 'Landlord', '2014-09-30', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 05:14:49', '2014-09-06 05:14:49'),
(37, NULL, 0, 'mamadou', 'l', 'jallow', 'Landlord', '2014-09-12', 'male', 'Gambia, The', 'Serere', NULL, NULL, 0, '2014-09-06 06:14:20', '2014-09-06 06:14:20'),
(38, NULL, 0, 'mama', 'land', 'lord', 'Landlord', '2014-09-17', 'male', 'Angola', 'Serere', NULL, NULL, 0, '2014-09-06 06:15:51', '2014-09-06 06:15:51'),
(39, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:17:23', '2014-09-06 06:17:23'),
(40, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:19:28', '2014-09-06 06:19:28'),
(41, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:20:43', '2014-09-06 06:20:43'),
(42, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:28:44', '2014-09-06 06:28:44'),
(43, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:31:53', '2014-09-06 06:31:53'),
(44, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:32:28', '2014-09-06 06:32:28'),
(45, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 06:33:07', '2014-09-06 06:33:07'),
(46, NULL, 0, 'afdsaf', 'adsfa', 'asdfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Manjago', NULL, NULL, 0, '2014-09-06 07:03:48', '2014-09-06 07:03:48'),
(47, NULL, 0, 'fsdfasdf', 'asdfa', 'asdfadsfa', 'Landlord', '2014-09-24', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 07:21:16', '2014-09-06 07:21:16'),
(48, NULL, 0, 'dfsadfa', 's', 'sdfasdfa', 'Landlord', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 07:30:11', '2014-09-06 07:30:11'),
(49, NULL, 0, 'asfas', 'asdfasd', 'asdfafa', 'Landlord', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 15:02:57', '2014-09-06 15:02:57'),
(50, NULL, 0, 'afdsfasdfa', 'dsf', 'sdfasdfadfa', 'Landlord', '2014-09-24', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 15:17:40', '2014-09-06 15:17:40'),
(51, NULL, 0, 'something', ' s ', 'sdf', 'Landlord', '2014-09-30', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 15:45:30', '2014-09-06 15:45:30'),
(52, NULL, 0, 'asdfasdf', '2sd', 'sdfsadfa', 'Landlord', '2014-09-10', 'female', 'Anguilla', 'Tukulor', NULL, NULL, 0, '2014-09-06 15:47:34', '2014-09-06 15:47:34'),
(53, NULL, 0, 'asdfasdf', '2sd', 'sdfsadfa', 'Landlord', '2014-09-10', 'female', 'Anguilla', 'Tukulor', NULL, NULL, 0, '2014-09-06 15:48:16', '2014-09-06 15:48:16'),
(54, NULL, 0, 'dfasdfa', 's', 'sfasdfa', 'Landlord', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 15:49:28', '2014-09-06 15:49:28'),
(55, NULL, 0, '', '', '', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:16:13', '2014-09-06 16:16:13'),
(56, NULL, 0, '', '', '', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:18:54', '2014-09-06 16:18:54'),
(57, NULL, 0, '', '', '', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:19:55', '2014-09-06 16:19:55'),
(58, NULL, 0, '', '', '', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:21:21', '2014-09-06 16:21:21'),
(59, NULL, 0, 'sdfa', 'asdf', 'asdf', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:30:48', '2014-09-06 16:30:48'),
(60, NULL, 0, 'sdfa', 'asdf', 'asdf', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:32:05', '2014-09-06 16:32:05'),
(61, NULL, 0, 'sdfa', 'asdf', 'asdf', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 16:33:20', '2014-09-06 16:33:20'),
(62, NULL, 0, 'lamin', 's', 'cessay', 'Landlord', '2014-09-30', 'male', 'Anguilla', 'Sarahule', NULL, NULL, 0, '2014-09-06 18:44:23', '2014-09-06 18:44:23'),
(63, NULL, 0, 'afsdfa', 'asdfa', 'asdfa', 'Landlord', '2014-09-30', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 18:48:07', '2014-09-06 18:48:07'),
(64, NULL, 0, 'papa', 's', 'secka', 'Landlord', '2014-10-01', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 19:03:13', '2014-09-06 19:03:13'),
(65, NULL, 0, 'papa', 's', 'secka', 'Landlord', '2014-10-01', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 19:04:33', '2014-09-06 19:04:33'),
(66, NULL, 0, 'ebrima', NULL, 'touray', 'Landlord', '2014-09-11', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 19:05:05', '2014-09-06 19:05:05'),
(67, NULL, 0, 'asdfda', 's', 'asdfa', 'Landlord', '2014-09-10', 'male', 'Gambia, The', 'Tukulor', NULL, NULL, 0, '2014-09-06 19:05:34', '2014-09-06 19:05:34'),
(68, NULL, 0, 'asdfasdfa', 'asdfadf', 'adsfadsfa', 'Landlord', '2014-09-17', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-06 20:58:09', '2014-09-06 20:58:09'),
(69, NULL, 0, 'mama', 'dou', 'jallow', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-06 22:05:44', '2014-09-06 22:05:44'),
(70, NULL, 0, 'asdfa', 'asdfas', 'sdfasdfaf', 'Tenant', '2014-09-24', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-07 03:45:54', '2014-09-07 03:45:54'),
(71, NULL, 0, 'asdfa', 'asdf', 'asdfa', 'Tenant', '2014-09-12', 'male', 'Gambia, The', 'Other', NULL, NULL, 0, '2014-09-07 03:46:34', '2014-09-07 03:46:34'),
(72, NULL, 0, 'asdfa', 'asdf', 'asdfa', 'Tenant', '2014-09-12', 'male', 'Gambia, The', 'Other', NULL, NULL, 0, '2014-09-07 03:48:38', '2014-09-07 03:48:38'),
(73, NULL, 0, 'asdfa', 'asdf', 'asdfa', 'Tenant', '2014-09-12', 'male', 'Gambia, The', 'Other', NULL, NULL, 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(74, NULL, 0, 'atem', 'nant', 'tenant', 'Tenant', '2014-09-29', 'male', 'Gambia, The', 'Mandinka', NULL, NULL, 0, '2014-09-09 22:50:59', '2014-09-09 22:50:59'),
(75, NULL, 0, 'modou', 's', 'jallow', 'Tenant', '2014-09-12', 'male', 'Armenia', 'Tukulor', NULL, NULL, 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(76, NULL, 0, 'tenant', 'something', 'else', 'Tenant', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-09 23:06:07', '2014-09-09 23:06:07'),
(77, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(78, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(79, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(80, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(81, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(82, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(83, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(84, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(85, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(86, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(87, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(88, NULL, 0, 'mamadou', 's', 'jallow', 'Tenant', '2014-09-18', 'female', 'Argentina', 'Manjago', NULL, NULL, 0, '2014-09-10 01:17:55', '2014-09-10 01:17:55'),
(89, NULL, 0, 'PaSara', 'Drammeh', 'Drammeh', 'Landlord', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-09-10 18:48:36', '2014-09-10 18:48:36'),
(90, NULL, 0, 'rent', 'test', 'account', 'Tenant', '2014-09-25', 'female', 'Angola', 'Mandinka', NULL, NULL, 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(91, NULL, 0, 'montly', 'fee', 'tenant', 'Tenant', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-12 06:55:48', '2014-09-12 06:55:48'),
(92, NULL, 0, 'montly', 'fee', 'tenant', 'Tenant', '2014-09-18', 'male', 'Gambia, The', 'Fula', NULL, NULL, 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `plots`
--

CREATE TABLE IF NOT EXISTS `plots` (
  `plot_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plot_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_price` decimal(28,3) NOT NULL DEFAULT '0.000',
  `plot_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_lon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_status` tinyint(1) NOT NULL DEFAULT '0',
  `plot_remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_availability` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plot_cusID` int(11) NOT NULL DEFAULT '0',
  `plot_agenID` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`plot_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `plots`
--

INSERT INTO `plots` (`plot_id`, `plot_name`, `plot_size`, `plot_price`, `plot_location`, `plot_lon`, `plot_lat`, `plot_number`, `plot_status`, `plot_remarks`, `plot_availability`, `plot_cusID`, `plot_agenID`, `created_at`, `updated_at`) VALUES
(1, 'dsasdfas', 'asdfa', 0.000, 'adfa', NULL, NULL, 'adfafa', 0, 'asdfasdfa', NULL, 0, 0, '2014-08-21 23:10:45', '2014-08-21 23:10:45'),
(2, 'name', 'size', 0.000, 'location', NULL, NULL, 'number', 0, 'remraks', NULL, 0, 0, '2014-08-21 23:18:19', '2014-08-21 23:18:19'),
(3, 'the first one', '260 X 100', 0.000, 'kololi', NULL, NULL, 'adsf654316a4', 0, 'this is not yet sole', NULL, 0, 0, '2014-08-24 01:21:33', '2014-08-24 01:21:33'),
(4, 'point two ', '200 X 264', 1.544, 'kololi', NULL, NULL, '5455454545', 0, 'this plot is empty bla bla', NULL, 0, 0, '2014-08-30 01:47:22', '2014-08-30 01:47:22'),
(5, 'the plot', '28 X 30', 28000.000, 'serrekunda', NULL, NULL, 'saa28000', 0, 'asfasdfasdfafafa', NULL, 0, 0, '2014-08-30 02:07:05', '2014-08-30 02:07:05');

-- --------------------------------------------------------

--
-- Table structure for table `rents`
--

CREATE TABLE IF NOT EXISTS `rents` (
  `rent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rent_houseID` int(11) NOT NULL DEFAULT '0',
  `rent_tenantID` int(11) NOT NULL DEFAULT '0',
  `rent_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `rent_nextpaydate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rent_lastpaydate` datetime DEFAULT NULL,
  `rent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rent_advance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `rent_balance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `rent_firstmonthpaid` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `rents`
--

INSERT INTO `rents` (`rent_id`, `rent_houseID`, `rent_tenantID`, `rent_monthlyFee`, `rent_nextpaydate`, `rent_lastpaydate`, `rent_type`, `rent_advance`, `rent_balance`, `rent_firstmonthpaid`, `deleted`, `created_at`, `updated_at`) VALUES
(4, 0, 1, 0.00, NULL, NULL, NULL, 0.00, 0.00, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 16, 0.00, NULL, NULL, NULL, 0.00, 0.00, NULL, 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(6, 1, 17, 0.00, NULL, NULL, NULL, 12000.00, 0.00, NULL, 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(7, 2, 18, 4524.00, NULL, NULL, NULL, 24000.00, 0.00, NULL, 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `rent_payments`
--

CREATE TABLE IF NOT EXISTS `rent_payments` (
  `paym_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paym_userID` int(11) NOT NULL DEFAULT '0',
  `paym_rentID` int(11) NOT NULL DEFAULT '0',
  `paym_houseID` int(11) NOT NULL DEFAULT '0',
  `paym_forMonths` int(11) NOT NULL DEFAULT '0',
  `paym_paidAmount` decimal(20,3) NOT NULL DEFAULT '0.000',
  `paym_balance` decimal(20,3) NOT NULL DEFAULT '0.000',
  `paym_currentBal` decimal(20,3) NOT NULL DEFAULT '0.000',
  `paym_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paym_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `entered_months` int(11) NOT NULL DEFAULT '0',
  `owing` decimal(8,2) NOT NULL DEFAULT '0.00',
  `monthsfrom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `monthsto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overpay` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paym_remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`paym_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `rent_payments`
--

INSERT INTO `rent_payments` (`paym_id`, `paym_userID`, `paym_rentID`, `paym_houseID`, `paym_forMonths`, `paym_paidAmount`, `paym_balance`, `paym_currentBal`, `paym_date`, `paym_deleted`, `entered_months`, `owing`, `monthsfrom`, `monthsto`, `overpay`, `created_at`, `updated_at`, `paym_remarks`) VALUES
(1, 1, 7, 2, 0, 2.000, 1000.000, 0.000, '2014-09-15', 0, 0, 0.00, NULL, NULL, 0.00, '2014-09-12 10:16:14', '2014-09-12 10:16:14', 'asdfadfa'),
(2, 1, 7, 2, 0, 2.000, 1000.000, 0.000, '2014-09-15', 0, 0, 0.00, NULL, NULL, 0.00, '2014-09-12 10:17:31', '2014-09-12 10:17:31', 'asdfadfa'),
(3, 1, 7, 2, 0, 2.000, 1000.000, 0.000, '2014-09-15', 0, 0, 0.00, NULL, NULL, 0.00, '2014-09-12 10:20:03', '2014-09-12 10:20:03', 'asdfadfa'),
(4, 1, 7, 2, 1, 6000.000, 1476.000, 0.000, '2014-09-12', 0, 0, 0.00, NULL, NULL, 0.00, '2014-09-12 10:22:00', '2014-09-12 10:22:00', 'this is his first rent payment');

-- --------------------------------------------------------

--
-- Table structure for table `tenants`
--

CREATE TABLE IF NOT EXISTS `tenants` (
  `tent_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tent_houseID` int(11) NOT NULL DEFAULT '0',
  `tent_compoundID` int(11) NOT NULL DEFAULT '0',
  `tent_rentID` int(11) NOT NULL DEFAULT '0',
  `tent_personid` int(11) NOT NULL DEFAULT '0',
  `tent_advance` decimal(8,2) NOT NULL DEFAULT '0.00',
  `tent_monthlyFee` decimal(8,2) NOT NULL DEFAULT '0.00',
  `tent_paymentStype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tent_status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tenants`
--

INSERT INTO `tenants` (`tent_id`, `tent_houseID`, `tent_compoundID`, `tent_rentID`, `tent_personid`, `tent_advance`, `tent_monthlyFee`, `tent_paymentStype`, `tent_status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 4, 6, 232.00, 0.00, NULL, 0, 0, '2014-09-07 03:50:42', '2014-09-07 03:50:42'),
(2, 1, 1, 0, 0, 220.00, 0.00, NULL, 0, 0, '2014-09-09 22:50:59', '2014-09-09 22:50:59'),
(3, 1, 1, 0, 0, 999999.99, 0.00, NULL, 0, 0, '2014-09-09 22:55:57', '2014-09-09 22:55:57'),
(4, 1, 1, 0, 76, 2500.00, 0.00, NULL, 0, 0, '2014-09-09 23:06:07', '2014-09-09 23:06:07'),
(5, 1, 1, 0, 77, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:09:08', '2014-09-10 01:09:08'),
(6, 1, 1, 0, 78, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:09:32', '2014-09-10 01:09:32'),
(7, 1, 1, 0, 79, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:09:44', '2014-09-10 01:09:44'),
(8, 1, 1, 0, 80, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:12:30', '2014-09-10 01:12:30'),
(9, 1, 1, 0, 81, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:13:29', '2014-09-10 01:13:29'),
(10, 1, 1, 0, 82, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:13:52', '2014-09-10 01:13:52'),
(11, 1, 1, 0, 83, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:14:31', '2014-09-10 01:14:31'),
(12, 1, 1, 0, 84, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:15:50', '2014-09-10 01:15:50'),
(13, 1, 1, 0, 85, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:16:14', '2014-09-10 01:16:14'),
(14, 1, 1, 0, 86, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:16:31', '2014-09-10 01:16:31'),
(15, 1, 1, 0, 87, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:16:46', '2014-09-10 01:16:46'),
(16, 1, 1, 0, 88, 200.00, 0.00, NULL, 0, 0, '2014-09-10 01:17:56', '2014-09-10 01:17:56'),
(17, 1, 1, 0, 90, 12000.00, 0.00, NULL, 0, 0, '2014-09-12 05:55:27', '2014-09-12 05:55:27'),
(18, 2, 0, 0, 92, 24000.00, 0.00, NULL, 0, 0, '2014-09-12 06:57:06', '2014-09-12 06:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `trans_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trans_custID` int(11) NOT NULL DEFAULT '0',
  `trans_payID` int(11) NOT NULL DEFAULT '0',
  `trans_currentBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_dueBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_totalBal` decimal(28,3) NOT NULL DEFAULT '0.000',
  `trans_status` int(11) NOT NULL DEFAULT '0',
  `trans_visible` tinyint(1) NOT NULL DEFAULT '1',
  `trans_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `link`, `lickId`, `visible`, `deleted`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'sulsira@hotmail.com', '$2y$10$.3b1uD.DrBeqn9feJ1FgPey/qWkDuOLbrw9XgJcUt2g7U/mSbDso6', 0, 0, 0, 0, 'A9Xn2xJzMyvAwzGDbuiUrVlBCwDyCtOVuJOuznGt2FxZFmtmCEdNhJCyEefB', '2014-08-23 02:23:14', '2014-09-12 11:04:06'),
(2, NULL, 'admin@test.com', '$2y$10$MIXwV.1AID5.KUwlUs.dXuD0sIWEiJNU0OkOLGorgAnOCvGv4Ez2m', 0, 0, 0, 0, 'Gd4t6ieZiMBlGXsqDSl0QHBWbd6y2yevtv46gn170Gt5peVWFHDhneG9KoUq', '2014-08-23 02:28:34', '2014-09-10 03:19:43'),
(3, NULL, 'suls@hot.com', NULL, 0, 0, 0, 0, NULL, '2014-08-29 20:49:12', '2014-08-29 20:49:12'),
(4, NULL, 'sulss@something.com', NULL, 0, 0, 0, 0, NULL, '2014-08-30 21:39:33', '2014-08-30 21:39:33'),
(5, NULL, 'adsfasd@dsaf.co', NULL, 0, 0, 0, 0, NULL, '2014-08-30 21:42:45', '2014-08-30 21:42:45'),
(6, NULL, 'asdfasdf@daf.com', '$2y$10$NotcVDSWE7cK9LrvnOBcveAzbiw7Z0KJc4GFsQiYNCY4uhUD4nAZC', 0, 0, 0, 0, NULL, '2014-08-30 21:44:22', '2014-08-30 21:44:22'),
(7, NULL, 'asdfa@dsfasd', '$2y$10$5FaQWsLjRQrvsEXZcWMb7.grWl7Qc/mBKw4QC0Wmu862luqIg6FsG', 0, 0, 0, 0, NULL, '2014-08-30 21:52:32', '2014-08-30 21:52:32'),
(8, NULL, 'sdfsad@dsaa', '$2y$10$UBMU5IOGsw7NI72KVfcUgeOyRV9lBbB2B6.T9fdLJn/6aVP1U.7Re', 0, 0, 0, 0, NULL, '2014-08-30 21:54:29', '2014-08-30 21:54:29'),
(9, NULL, 'sulsira@hotmail.com3', '$2y$10$L8tvS6hXfCW5fo9pW8YWVeO5io1D6jYFDg2dOdtAyhRgXCteNdfW6', 0, 0, 0, 0, NULL, '2014-08-30 23:11:15', '2014-08-30 23:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `privileges` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `security_level` int(11) NOT NULL DEFAULT '0',
  `link` tinyint(1) NOT NULL DEFAULT '0',
  `lickId` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `user_id`, `type`, `fullname`, `ip`, `pc`, `privileges`, `domain`, `department_id`, `userGroup`, `security_level`, `link`, `lickId`, `url`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'mamadou jallow', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-23 02:23:14', '2014-08-23 02:23:14'),
(2, 2, NULL, 'everyone account', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-23 02:28:34', '2014-08-23 02:28:34'),
(3, 3, NULL, 'mamadou jallow', '0', NULL, 'veda', NULL, NULL, 'agent', 0, 0, 0, NULL, 0, '2014-08-29 20:49:12', '2014-08-29 20:49:12'),
(4, 4, NULL, 'saiho somehting', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 21:39:33', '2014-08-30 21:39:33'),
(5, 5, NULL, 'dfadsf afadf', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 21:42:45', '2014-08-30 21:42:45'),
(6, 6, NULL, 'sda asdfasdfa', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 21:44:22', '2014-08-30 21:44:22'),
(7, 7, NULL, 'ewrwa adsasfas', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 21:52:32', '2014-08-30 21:52:32'),
(8, 8, NULL, 'saf adsfa adsfasdf', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 21:54:29', '2014-08-30 21:54:29'),
(9, 9, NULL, 'sulsira  sdfm', '0', NULL, 'veda', NULL, NULL, 'admin', 0, 0, 0, NULL, 0, '2014-08-30 23:11:15', '2014-08-30 23:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE IF NOT EXISTS `variables` (
  `Vari_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vari_VariableName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vari_Field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Vari_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=502 ;

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`Vari_ID`, `Vari_VariableName`, `Vari_Table`, `Vari_Field`, `created_at`, `updated_at`, `Deleted`) VALUES
(50, 'University', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(51, 'College', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(52, 'Vocational', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(53, 'Technical', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(54, 'Other', 'LearningCenter', 'LeCe_InstitutionType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(55, 'Public', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(56, 'Private', 'LearningCenter', 'LeCe_Ownership', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(58, 'Full Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(59, 'Part Time', 'Class', 'Clas_AttendanceStatus', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(60, 'Male', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(61, 'Female', 'Student', 'Stud_Sex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(62, 'GABECE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(63, 'WASSCE', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(64, 'O ', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(65, 'A', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(66, 'Pre-Entry', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(67, 'Bachelor', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(68, 'Government', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(69, 'Private', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(70, 'NGO', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(71, 'Other', 'Student', 'Stud_Sponsor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(76, 'Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(77, 'Assoc. Professor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(78, 'Senior Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(79, 'Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(80, 'Asst. Lecturer', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(82, 'Chief Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(83, 'Principal Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(84, 'Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(85, 'Asst. Tutor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(91, 'Admin/Support- Full Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(92, 'Admin/Support- Part Time', 'Staff', 'Staff_Role/Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(93, 'Learning Center', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(94, 'Student', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(95, 'Staff', 'EntityType', 'EntityType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(96, 'Mobile', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(97, 'LAN Line', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(98, 'FAX', 'Phone', 'PhoneType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(100, 'Gambia, The', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(101, 'Senegal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(102, 'Algeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(103, 'Angola', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(104, 'Anguilla', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(105, 'Antigua and Barbuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(106, 'Argentina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(107, 'Armenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(108, 'Aruba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(109, 'Australia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(110, 'Austria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(111, 'Azerbaijan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(112, 'Bahamas', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(113, 'Bahrain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(114, 'Bangladesh', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(115, 'Barbados', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(116, 'Belarus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(117, 'Belgium', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(118, 'Belize', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(119, 'Benin', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(120, 'Bermuda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(121, 'Bhutan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(122, 'Bolivia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(123, 'Bosnia and Herzegovina', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(124, 'Botswana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(125, 'Brazil', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(126, 'British Virgin Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(127, 'Brunei Darussalam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(128, 'Bulgaria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(129, 'Burkina Faso', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(130, 'Burundi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(131, 'Cambodia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(132, 'Cameroon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(133, 'Canada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(134, 'Cape Verde', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(135, 'Cayman Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(136, 'Central African Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(137, 'Chad', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(138, 'Chile', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(139, 'China', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(140, 'Colombia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(141, 'Comoros', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(142, 'Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(143, 'Cook Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(144, 'Costa Rica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(145, 'C', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(146, 'Croatia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(147, 'Cuba', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(148, 'Cyprus', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(149, 'Czech Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(150, 'Demo. Repub. of the Congo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(151, 'Denmark', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(152, 'Dominica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(153, 'Dominican Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(154, 'Ecuador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(155, 'Egypt', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(156, 'El Salvador', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(157, 'Equatorial Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(158, 'Eritrea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(159, 'Estonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(160, 'Ethiopia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(161, 'EUROPE not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(162, 'Fiji', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(163, 'Finland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(164, 'France', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(165, 'Gabon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(167, 'Georgia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(168, 'Germany', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(169, 'Ghana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(170, 'Gibraltar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(171, 'Greece', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(172, 'Grenada', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(173, 'Guatemala', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(174, 'Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(175, 'Guinea-Bissau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(176, 'Guyana', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(177, 'Haiti', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(178, 'Holy See', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(179, 'Honduras', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(180, 'Hong Kong (SAR)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(181, 'Hungary', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(182, 'Iceland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(183, 'India', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(184, 'Indonesia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(185, 'Iran (Islamic Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(186, 'Iraq', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(187, 'Ireland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(188, 'Israel', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(189, 'Italy', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(190, 'Jamaica', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(191, 'Japan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(192, 'Jordan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(193, 'Kazakhstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(194, 'Kenya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(195, 'Kiribati', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(196, 'Korea (Democratic People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(197, 'Korea (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(198, 'Kuwait', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(199, 'Kyrgyzstan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(200, 'Lao People', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(201, 'Latvia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(202, 'Lebanon', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(203, 'Lesotho', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(204, 'Liberia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(205, 'Libyan Arab Jamahiriya', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(206, 'Liechtenstein', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(207, 'Lithuania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(208, 'Luxembourg', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(209, 'Macao (China)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(210, 'Madagascar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(211, 'Malawi', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(212, 'Malaysia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(213, 'Maldives', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(214, 'Malta', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(215, 'Marshall Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(216, 'Mauritania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(217, 'Mauritius', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(218, 'Mexico', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(219, 'Micronesia (Federal States of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(220, 'Moldova (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(221, 'Monaco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(222, 'Mongolia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(223, 'Montserrat', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(224, 'Morocco', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(225, 'Mozambique', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(226, 'Myanmar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(227, 'Namibia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(228, 'Nauru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(229, 'Nepal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(230, 'Netherlands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(231, 'Netherlands Antilles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(232, 'New Zealand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(233, 'Nicaragua', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(234, 'Niger', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(235, 'Nigeria', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(236, 'Niue', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(237, 'NORTH AMERICA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(238, 'Norway', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(239, 'OCEANIA not specified', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(240, 'Oman', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(241, 'Pakistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(242, 'Palau (Republic of)', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(243, 'Palestinian Autonomous Territories', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(244, 'Panama', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(245, 'Papua New Guinea', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(246, 'Paraguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(247, 'Peru', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(248, 'Philippines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(249, 'Poland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(250, 'Portugal', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(251, 'Qatar', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(252, 'Romania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(253, 'Russian Federation', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(254, 'Rwanda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(255, 'Saint Kitts and Nevis', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(256, 'Saint Vincent and the Grenadines', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(257, 'Samoa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(258, 'San Marino', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(259, 'Sao Tome and Principe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(260, 'Saudi Arabia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(262, 'Serbia and Montenegro', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(263, 'Seychelles', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(264, 'Sierra Leone', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(265, 'Singapore', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(266, 'Slovakia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(267, 'Slovenia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(268, 'Solomon Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(269, 'Somalia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(270, 'South Africa', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(271, 'Spain', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(272, 'Sri Lanka', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(273, 'Sudan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(274, 'Suriname', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(275, 'Swaziland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(276, 'Sweden', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(277, 'Switzerland', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(278, 'Syrian Arab Republic', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(279, 'Tajikistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(280, 'Thailand', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(281, 'Republic of Macedonia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(282, 'Timor-Leste', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(283, 'Togo', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(284, 'Tokelau', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(285, 'Tonga', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(286, 'Trinidad and Tobago', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(287, 'Tunisia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(288, 'Turkey', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(289, 'Turkmenistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(290, 'Turks and Caicos Islands', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(291, 'Tuvalu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(292, 'Uganda', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(293, 'Ukraine', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(294, 'United Arab Emirates', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(295, 'United Kingdom', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(296, 'United Republic of Tanzania', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(297, 'United States of America', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(298, 'Uruguay', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(299, 'Uzbekistan', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(300, 'Vanuatu', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(301, 'Venezuela', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(302, 'Viet Nam', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(303, 'Yemen', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(304, 'Zambia', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(305, 'Zimbabwe', 'Country', 'Country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(306, 'Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(307, 'Ordinary Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(308, 'Higher Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(309, 'Active', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(310, 'Inactive', 'Standing', 'Standing', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(311, 'Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(312, 'Ordinary Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(313, 'Bachelor', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(314, 'Bachelor', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(315, 'Post Graduate Certificate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(316, 'Post Graduate Diploma', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(317, 'Masters', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(318, 'Doctorate', 'Course', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(320, '6', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(321, '12', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(322, '18', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(323, '24', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(324, '36', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(325, '48', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(326, '84', 'Programs', 'Prog_Duration', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(327, 'Other', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(328, 'Advanced Diploma\r\nOrdinary Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(329, 'Bachelor', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(330, 'Masters', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(331, 'Horticulture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(332, 'Electrical Installation', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(333, 'Carpentry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(334, 'Building Construction', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(335, 'Surveying', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(336, 'Food Processing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(337, 'Hospitality', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(338, 'Architecture', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(339, 'Hairdressing', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(340, 'Animal Husbandry', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(341, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(343, 'Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(344, 'Diploma', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(345, 'Student', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(346, 'Staff', 'Person', 'Pers_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(347, 'Motor Vehicle Systems', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(349, 'Plumbing and Gas Fitting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(350, 'Cooking and Baking', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(351, 'Sewing/Craft Work and Tie Dye', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(352, 'Home Economics', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(353, 'House Keeping', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(354, 'Refrigeration and Air Conditioning', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(355, 'Painting', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(356, 'Graphic Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(357, 'Draftsmanship', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(358, 'Secretarial', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(359, 'Computer Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(360, 'Fashion Design', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(361, 'Mobile Repair', 'Student', 'Stud_GSQ-OccupationalArea', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(362, '1', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(363, '2', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(364, '3', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(365, '4', 'Student', 'Stud_GSQ-Level', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(366, 'Basic Programmes', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(367, 'Teacher training and education science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(368, 'Education Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(369, 'Arts', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(370, 'Humanities', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(371, 'Social and Behavioural Science', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(372, 'Journalism and Information', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(373, 'Business and Administration', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(374, 'Law', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(375, 'Life Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(376, 'Physical Sciences', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(377, 'Mathematics and Statistics', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(378, 'Computing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(379, 'Engineering and Engineering Trades', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(380, 'Manufacturing and Processing', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(381, 'Architecture and Building', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(382, 'Agriculture, Forestry and Fishery', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(383, 'Veterinary', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(384, 'Health', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(385, 'Social Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(386, 'Personal Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(387, 'Transport Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(388, 'Environmental Protection', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(389, 'Security Services', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(390, 'Unspecified', 'Student', 'Stud_Major', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(391, 'General Programmes', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(392, 'Education', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(393, 'Humanities and Arts', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(394, 'Social Sciences', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(395, 'Physical and Natural Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(396, 'Engineering, Manufacturing and Construction', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(397, 'Agriculture', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(398, 'Public and Medical Health Science', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(399, 'Tourism/Hospitality/Services', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(400, 'Not Known or Unspecified', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(401, 'Permanent', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(402, 'Temporary', 'Staff', 'Staff_EmploymentType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(403, 'Masters', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(404, 'Doctorate (PhD)', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(405, 'Post Graduate Certificate', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(406, 'Administraive staff', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(407, 'Instructor', 'Staff', 'Staff_Rank', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(408, 'J.S.S Certificate', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(409, 'Business', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(410, 'Law', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(412, 'Information and Communication Technology', 'Student', 'Stud_Specialization', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(413, 'Basic Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(414, 'Certificate', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(415, 'Diploma	Student', 'Programs', 'Prog_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(416, 'Bachelor', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(417, 'Masters	Student', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(418, 'Doctorate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(419, 'Post Graduate Certificate', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(420, 'Post Graduate Diploma', 'Courses', 'Cour_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(421, 'Phone', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(422, 'Email', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(423, 'Academic', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(424, 'Administrative', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(425, 'Both', 'Staff', 'Staff_Role', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(426, 'Post Graduate Diploma', 'Staff', 'Staff_Qualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(430, 'Banjul South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(431, 'Banjul Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(432, 'Banjul North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(433, 'Serre Kunda East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(434, 'Serre Kunda West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(435, 'Serre Kunda Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(436, 'Bakau', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(437, 'Kombo North', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(438, 'Kombo South', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(439, 'Kombo Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(440, 'Kombo East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(441, 'Foni Brefet', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(442, 'Foni Bintang', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(443, 'Foni Kansalla', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(444, 'Foni Bondali', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(445, 'Foni Jarrol', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(446, 'Kiang West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(447, 'Kiang Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(448, 'Kiang East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(449, 'Jarra West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(450, 'Jarra Central', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(451, 'Jarra East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(452, 'Lower Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(453, 'Upper Nuimi', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(454, 'Jokadu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(455, 'Lower Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(456, 'Central Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(457, 'Upper Baddibu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(458, 'Niani', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(459, 'Lower Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(460, 'Upper Saloum', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(461, 'Nianija', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(462, 'Sami', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(463, 'Niamina West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(464, 'Niamina East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(465, 'Niamina Dankunko', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(466, 'Janjanbureh', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(467, 'Fuladu East', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(468, 'Kantora', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(469, 'Wuli', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(470, 'Sandu', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(471, 'Fuladu West', 'Address', 'Addr_District', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(475, 'Web site', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(476, 'Fula', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(477, 'Jola', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(478, 'Mandinka', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(479, 'Manjago', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(480, 'Sarahule', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(481, 'Serere', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(482, 'Tukulor', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(483, 'Wollof', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(484, 'Other', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(485, 'Fax', 'ContactInfo', 'Cont_ContactType', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(486, 'N/A', 'Person', 'Pers_Ethnicity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(487, 'Masters', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(488, 'Doctorate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(489, 'Post Graduate Certificate', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(490, 'Post Graduate Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(491, 'Higher Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(492, 'Advanced Diploma', 'Class', 'Class_Award', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(493, 'Higher Education', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(494, 'Tertiary', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(495, 'TVET', 'LearningCenter', 'LeCe_Classification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(496, 'Internal', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(497, 'External', 'Users', 'User_Type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(498, 'Basic Literacy', 'Class', 'Clas_EntryQualification', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(499, 'Public', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(500, 'Government', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(501, 'Both', 'LearningCenter', 'LeCe _FinancialSource', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
