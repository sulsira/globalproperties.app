 <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
          
          <div class="item active">
            <div class="container">
             <div class="row featurette">
              <div class="col-md-6">
                 <h2 class="featurette-heading">MEET <abbr title="Higher Education Information Management System">HEIMS</abbr>. <span class="text-muted">One app all data.</span></h2>
                  <p class="lead">
                    For all Higher Education Data in one place,real time.
                  </p>
                  <p><a class="btn btn-large btn-primary featurette-btn" href="about-hemis.php">Get Started</a></p>
                </div>
                <div class="col-md-6">
                  <img class="featurette-image img-responsive" src="__public/img/hemis.jpg" alt="Generic placeholder image">
                </div>
                
              </div>
            </div>
          </div>

           <div class="item">
            <div class="container">
             <div class="row featurette">
              <div class="col-md-6">

                  <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
                  <p class="lead">Donec ullamcorper nulla non metus auctor fringilla.</p>
                </div>
                <div class="col-md-6">
                  <img class="featurette-image img-responsive" src="__public/img/hemis.jpg" alt="Generic placeholder image">
                </div>
                
              </div>
            </div>
          </div>

        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
      </div><!-- /.carousel -->