<?php

class Tenant extends \Eloquent {
	protected $primaryKey = 'tent_id';
	protected $fillable = [
'tent_id',
'tent_houseID',
'tent_compoundID',
'tent_rentID',
'tent_personid',
'tent_advance',
'tent_monthlyFee',
'tent_paymentStype',
'tent_status'  # 0 - left, 1 = mean occupying, 2 = pending to leave, 3 = owed rent before leaving; 4 = does not have a house
];

	public function house(){
		return $this->belongsTo('House','tent_houseID','hous_id');
	}
	public function person(){
		return $this->belongsTo('Person','tent_personid','id');
	}
	public function rents(){
		return $this->hasMany('Rent','rent_tenantID','tent_id');
	}
	public function documents(){
		return $this->hasMany('Document','entity_ID','tent_id');
	}
}