<?php namespace services\validators;

class Staff extends Validate{
		public static $rules = [
		'hq'=> 'max:200',
		'mtf'=> 'max:200',
		'role'=> 'max:200',
		'rank'=> 'max:200'
	];
	public function __construct($attributes = null){
		$this->attributes = $attributes ?: \Input::all();
	}
}