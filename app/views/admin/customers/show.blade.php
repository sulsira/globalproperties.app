<?php #page specific processing
$image = array();
$person =  array();
$contacts = array();
$documents =  array();
$fullname = '';
$customer_id = $customer['cust_id'];
$kin = (!empty($customer['kin']))? $customer['kin'] : [];
    if(isset($customer) && !empty($customer)):
        $person = $customer['person'];
        $addresses = $person['addresses'];
        $contacts = $customer['person']['contacts'];
        $fullname = ucwords($person['pers_fname'] .'  '. $person['pers_mname'].' '.$person['pers_lname']);
        foreach ($customer as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
        }
    endif;

    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }

 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-landpayment')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="">Customer Name : {{ucwords("{ $fullname }")}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="#basic">General</a> </li>
                            <li><a href="#transactions">Transactions</a></li>
                            <li><a href="#payments">Payments</a></li>
                            <li><a href="#plots">Plots</a></li>
                            <li><a href="{{route('customers.edit',$customer['cust_id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul> 

                <?php endif ?>  
            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($fullname)}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Plots</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($customer['plots'])): ?>
                                          <?php foreach ($customer['plots'] as $key => $plot): ?>
                                            <tr>
                                                <td>name:</td>
                                                <td>{{ucwords($plot['plot_name'])}}</td>
                                                <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                            </tr>                                                
                                            <?php endforeach ?>  
                                        <?php endif ?>
                                    </tbody>
                                </table>
    <table class="table table-condensed table-hover">
        <thead>
            <tr>
                <th colspan="3">Next of kin</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($kin)): ?>

                        <tr>
                            <td>Fullname:</td>
                            <td><?php echo ucwords($kin['fname'].' '.$kin['mname'].' '.$kin['lname'] ) ?></td>
                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                        </tr>
                        <tr>
                            <td>Contacts: </td>
                            <td>{{$kin['contacts']}}</td>
                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                        </tr>                                                      
            <?php endif ?>
        </tbody>
    </table>
                            </div>
                </div>

            </div> <!-- a .cc -->
  <div class="cc clearfix" id="transactions">
                <div class="messages">
                    @include('flash::message')
                    @include('__partials/errors')
                    <hr>
                </div>
    <h3>Transactions Records</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>plot</th>
            <th>plot price</th>
            <th>Down payment</th>
            <th>Paid total</th>
            <th>Remainig Balance</th>
            <th>Monthly Fee</th>
            <th>Period</th>
            <th>Commencing</th>
            <th>Payment Type</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer)): ?>
          <?php foreach ($customer['plots'] as $p => $plot): ?>
            <tr>
                <td>
                    <a href="{{route('plots.show',$plot['plot_id'])}}">{{$plot['plot_number']}}</a>
                </td> 
                <td>{{$plot['plot_price']}}</td> 
                <td>{{$customer['cust_downpayment']}}</td> 
                <td>{{$customer['total_payment']}}</td> <!-- his payment from start including downpayment-->
                <td>{{$customer['Balance']}}</td> <!-- the amount of money left for him to pay -->
               
                <td>{{$customer['Monthly_Fee']}}</td> <!-- How much he should pay everymonth -->
                 <td>{{$customer['period']}}</td> <!-- How long the payments should last -->
                <td>{{$customer['Commencing']}}</td> <!-- Monthly payment should start -->
                <td>{{$customer['pay_type']}}</td> <!-- payment Type -->
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>no transactions calculated yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
   <div class="cc clearfix" id="payments">
        <h3>Payments  <a href="#landpayment" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>plot</th>
            <th>Amount</th>
            <th>recorded by</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
           
                
                <?php if (!empty($plot['payments'])): ?>
                    <?php foreach ($plot['payments'] as $p => $pay): ?>
                     <tr>
                    <td><a href="{{route('plots.show',$plot['plot_id'])}}">{{ucwords($plot['plot_number'])}}</a></td>
                            <td>{{e($pay['paym_paidAmount'])}}</td>                       
                            <td>{{e($pay['user']['email'])}}</td>                       
                            <td>{{e($pay['created_at'])}}</td>  
                         </tr>                     
                    <?php endforeach ?> 
                    <?php else: ?>
                    <td colspan="2">no payment</td>                    
                <?php endif ?>
                                                         
            <?php endforeach ?>
            <?php else: ?>
            <tr><td colspan="5"><h4>no payments made yet!</h4></td></tr>  
        <?php endif ?>
      </tbody>

    </table>

   </div>
   <div class="cc clearfix" id="plots">
        <h3>Plots <a href="#add-plot" role="button" data-toggle="modal"> <i class="fa fa-plus"></i></a></h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>plot number</th>
            <th>plot size</th>
            <th>plot price</th>
            <th>location</th>
            <th>status</th>
            <th>availability</th>
            <th>remarks</th>
            <th>created</th>
            <th>action</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['plots'])): ?>
          <?php foreach ($customer['plots'] as $key => $plot): ?>
            <tr>
                <td>{{$plot['plot_number']}}</td>
                <td>{{$plot['plot_size']}}</td>
                <td>{{$plot['plot_price']}}</td>
                <td>{{$plot['plot_location']}}</td>
                <td>{{$plot['plot_status']}}</td>
                <td>{{$plot['plot_availability']}}</td>
                <td>{{$plot['plot_remarks']}}</td>
                <td>{{$plot['created_at']}}</td>
                <td><a href="{{route('plots.show',$plot['plot_id'])}}">view</a> | <a href="#">options</a></td>
            </tr>                                                
            <?php endforeach ?>  
        <?php else: ?>
        <tr rowspan="7"><td>No plots recorded</td></tr>
        <?php endif ?>
      </tbody>
    </table>
   </div>
</div>


@stop
@include('templates/bottom-admin')