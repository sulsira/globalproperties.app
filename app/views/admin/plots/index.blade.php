<?php #page specific processing ?>
@include('templates/top-admin')
@section('content')
	<div class="c-header cc">
		<h3>Plots</h3>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Plot Name</th>
					<th>Plot Size</th>
					<th>Plot Price</th>
					<th>Plot Customer</th>
					<th>Plot Location</th>
					<th>Plot Created</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($plots)): ?>
					<?php foreach ( $plots as $key => $value ): ?>
						<tr>
							<td>{{ucwords($key + 1)}}</td>
							<td>{{ucwords($value['plot_name'])}}</td>
							<td>{{ucwords($value['plot_size'])}}</td>
							<td>{{ucwords($value['plot_price'])}}</td>
							<td>
								<a href="{{route('customers.show',$value['customer']['cust_id'])}}"><?php echo ucwords($value['customer']['person']['pers_fname'] .'  '. $value['customer']['person']['pers_mname'].' '.$value['customer']['person']['pers_lname']) ?></a>
							</td>
							<td>
								{{ucwords($value['plot_location'])}}
							</td>
							<td>{{ucwords($value['created_at'])}}</td>	
							<td>
								<a href="{{route('plots.show',$value['plot_id'])}}">view</a> |

								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">
									Options
									<!-- <span class="caret"></span> -->
								</a>
								  <ul class="dropdown-menu">
								    <!-- dropdown menu links -->
								    <li><a href="{{route('plots.edit',$value['plot_id'])}}">edit</a></li>
								    <li>
								    	{{Form::delete('plots/'. $value['plot_id'], 'Delete')}}
								    </li>
								  </ul>
								
							</td>
						</tr>
					<?php endforeach ?>
					<?php else: ?>
					<tr>
						<td colspan="6"><h4>No Plot!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')