@include('templates/top-admin')
@section('content')
	<div class="cc">
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				{{Form::model($plot,['route'=>['plots.update',$plot->plot_id],'method'=>'PATCH'],['class'=>'form-snippet'])}}
					<div class="level name">
						<div>
							{{Form::label('plot_location','Location')}}
							{{Form::text('plot_location',null,['class'=>'input-xlarge span6','placeholder'=>'Location or Adress'])}}
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_price','Plot Price (In Dalasis)')}}
							{{Form::text('plot_price',null,['class'=>'input-xlarge span6','placeholder'=>'Enter the Plot price'])}}
						</div>
						<div>
							{{Form::label('plot_size','Plot Size')}}
								<select name="plot_size" class="span6">
									<option selected="1">{{$plot->plot_size}}</option>
									<?php $plots = Variable::domain('plot_size')->toArray();  ?>
									@foreach ($plots as  $key => $plott)						
										<option>{{$plott['Vari_VariableName']}}</option>
									@endforeach
								</select>
						</div>
					</div>
					<div class="level name">
						<div>
							{{Form::label('plot_number','Plot Number')}}
							{{Form::text('plot_number',null,['class'=>'input-xlarge span12','placeholder'=>'A unique Plot number'])}}
						</div>

					</div>
					<div class="level">
						<div>
							{{Form::label('plot_remarks','Plot Remarks')}}
							{{Form::textarea('plot_remarks',null,['class'=>'input-xlarge span12','placeholder'=>'Enter your remarks here'])}}
						</div>
					</div>
					<div class="level actions">
						<div>
							  <button type="submit" class="btn btn-large btn-primary span12" name="save" value="save">Save Changes</button>
						</div>
					</div>
				{{Form::close()}}
	</div>
@stop
@include('templates/bottom-admin')
