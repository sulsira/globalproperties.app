@include('templates/top-admin')
@section('content')
@include('__partials/modal-add-landlord')
	<div class="c-header cc">
		<h3>Landlords</h3>
	</div>
	<div class="cc">
				<div class="messages">
					@include('flash::message')
					@include('__partials/errors')
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Fullname</th>
							<th>Contacts</th>
							<th>Compounds</th>
							<th>created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($landlords)): ?>
							<?php foreach ($landlords as $key => $value): ?>
							<tr>
								<td>
									<a href="{{route('land-lords.show',$value['id'])}}">{{ucwords(e($value['ll_fullname']))}}</a>
								</td>
								<td>
									<?php if (!empty($value['person']['contacts'])): ?>
										<ul><?php foreach ($value['person']['contacts'] as $ind => $cont): ?>
										
												<li><span>{{e($cont['Cont_ContactType'])}} : </span> <strong>{{e($cont['Cont_Contact'])}}</strong></li>
										<?php endforeach ?></ul>
									<?php else: ?>
									N/A
									<?php endif ?>
								</td>
								<td>
									<?php if (!empty($value['compounds'])): ?>
										<ul>
											<?php foreach ( $value['compounds'] as $ind => $compound ): ?>
												<li>
													<?php if ( empty( $compound['comp_indentifier'] ) ): ?>
														<a href="{{route('houses.show',$compound['comp_id'])}}">
															# of Houses : {{$compound['comp_numberOfHouses']}}
														</a>
														<?php else: ?>
														<a href="{{ route('compounds.show',$compound['comp_id']) }} ">
															{{ ucwords( $compound['comp_indentifier'] ) }}
														</a>
													<?php endif ?>
												</li>
											<?php endforeach ?>
										</ul>
										<?php else: ?>
										no compounds
									<?php endif; ?>
								</td>
								<td>
									{{e($value['created_at'])}}
								</td>

								<td class="actions">
									<a href="{{route('land-lords.show',$value['id'])}}">view</a> |
									
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">
									Options
									<!-- <span class="caret"></span> -->
								</a>
								  <ul class="dropdown-menu">
								    <!-- dropdown menu links -->
								    <li><a href="{{route('land-lords.edit',$value['id'])}}">edit</a></li>
								    <!-- <li><a href="{{route('compounds.edit',$value['id'])}}">add Compound to</a></li> -->
								    <li>
								    	{{Form::delete('land-lords/'. $value['id'], 'Delete')}}
								    </li>
								  </ul>
								</td>
							</tr>						
							<?php endforeach ?>
							<?php else: ?>
							<tr>
								<td colspan="8"><h4>No Land lords Available!</h4></td>
							</tr>
						<?php endif ?>
					</tbody>
				</table>

		  </div>
		</div>
	</div>
@stop
@include('templates/bottom-admin')