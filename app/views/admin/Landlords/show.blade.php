<?php #page specific processing
    $image = array();
    $person =  array();
    $documents =  array();
    $compounds = array();
    $kin = array();
    if (!empty($landlord)) {
        foreach ($landlord as $key => $value) {
           if ($key == 'person') {
              $person = $value;
           }
           if ($key == 'documents') {
               foreach ($value as $d => $doc) {
                    if ($doc['type'] == 'Photo') {
                       $image = $doc;
                    }else{
                        $documents[] = $doc;
                    }
               }
           }
           if ($key == 'compounds') {
               $compounds = $value;
           }
           if ($key == 'kin') {
               $kin = $value;
           }
        }
    }
    $contacts = (!empty($person['contacts']))? $person['contacts'] : [];
    $addresses = (!empty($person['addresses']))? $person['addresses'] : [];

    if (!empty($person['documents'])) {
       foreach ($person['documents'] as $d => $doc) {
            if ($doc['type'] == 'Photo') {
               $image = $doc;
            }else{
                $documents[] = $doc;
            }
       }
    }
    // var_dump($kin);
    // die();
 ?>
@include('templates/top-admin')
@section('content')
   <div class="scope">
        <div class="hedacont">
            <div class="navbar">
                <div class="navbar-inner" id="scopebar">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target="navbar-responsive-collapse">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </a>
                        <a class="brand" href="{{route('land-lords.show',$landlord['id'])}}">Land lord Name : {{ucwords( $landlord['ll_fullname'] )}}</a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                          <ul class="nav">  
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#basic">General</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#compounds">Properties</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#houses">Houses</a> </li>
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#tenants">Tenants</a> </li>
                            <!-- <li><a href="#transactions">Transactions</a></li> -->
                            <li><a href="{{route('land-lords.show',$landlord['id'])}}#payments">Payments</a></li>
                            <li><a href="{{route('land-lords.edit',$landlord['id'])}}">Edit</a> </li>
                           </ul>
                        </div><!-- /.nav-collapse -->
                    </div>
                </div><!-- /navbar-inner -->
            </div> 
            <div class="c-header">
                <?php if (!empty($image)): ?>
                    <ul class="thumbnails" id="thmb">
                        <li class="span2">
                          <a href="#" class="thumbnail">
                           {{HTML::image($image['thumnaildir'])}}
                          </a>
                        </li>
                    </ul>                      
                <?php endif ?>

            </div>           
        </div>  
    </div>  <!-- end of scope -->

    <div class="content-details clearfix">
            <div class="cc clearfix" >
                <hr>
                <h3>Basic information</h3>
                <hr id="basic">
                <div class="span8 clearfix">
                            <div class="row">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">General information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Fullname:</td>
                                            <td>{{ucwords($landlord['ll_fullname'] )}}</td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Birth day:</td>
                                            <td>{{ucwords($person['pers_DOB'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td> {{ucwords($person['pers_gender'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Nationality:</td>
                                            <td> {{ucwords($person['pers_nationality'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>Ethniticity:</td>
                                            <td> {{ucwords($person['pers_ethnicity'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                        <tr>
                                            <td>National ID's:</td>
                                            <td> {{ucwords($person['pers_NIN'])}} </td>
                                            <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                        </tr>
                                    </tbody>

                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Contact Information</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($contacts)): ?>
                                            <?php foreach ($contacts  as $key => $value): ?>
                                                <tr>
                                                    <td>{{ucwords($value['Cont_ContactType'])}}:</td>
                                                    <td>{{ucwords($value['Cont_Contact'])}}</td>
                                                    <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>                                                
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($addresses)): ?>
                                            <?php foreach ($addresses as $key => $value): ?>
                                                <?php if (!empty($value)): ?>
                                                    <tr>
                                                        <td>Street: </td>
                                                        <td>{{$value['Addr_AddressStreet']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Town: </td>
                                                        <td>{{$value['Addr_Town']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>District: </td>
                                                        <td>{{$value['Addr_District']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                        
                                                <?php endif ?>
                                            <?php endforeach ?>
                                          
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Next of kin</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($kin)): ?>

                                                    <tr>
                                                        <td>Fullname:</td>
                                                        <td><?php echo ucwords($kin['fname'].' '.$kin['mname'].' '.$kin['lname'] ) ?></td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contacts: </td>
                                                        <td>{{$kin['contacts']}}</td>
                                                        <td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
                                                    </tr>                                                      
                                        <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div> <!-- a .cc -->
  <div class="cc clearfix" id="compounds">
    <h3>Properties</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Property</th>
            <th>Number of houses</th>
            <th>Location</th>
            <!-- <th>Available Houses</th> -->
            <th>created</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($compounds)): ?>
          <?php foreach ($compounds as $comp => $compound): ?>
            <tr>
                <td>{{ucwords($compound['name'])}}</td> 
                <td><?php echo count($compound['houses']) ?></td> 
                <td>{{ucwords($compound['location'])}}</td> 
                <td>{{$compound['created_at']}}</td> 
                <td>
                    <a href="{{route('compounds.show',$compound['comp_id'])}}">view</a> |
                    <a href="#">options</a>
                </td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>No properties yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
  <div class="cc clearfix" id="houses">
    <h3>Houses</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Property</th>
            <th>House #</th>
            <th>Tenant (current)</th>
            <th>Status</th>
            <th>price(D)</th>
            <th>Actions</th>
        </tr>
      </thead>
      <tbody>

        <?php if (!empty($compounds)): ?>

            <?php foreach ($compounds as $comp => $compound): ?>

                <?php if (!empty($compound['houses'])): ?>


                    <?php foreach ($compound['houses'] as $h => $house): ?>
                    
                        <tr>

<td><a href="{{route('compounds.show',$compound['comp_id'])}}">{{ucwords($compound['name'])}}</a></td> 

<td><a href="{{route('houses.show',$house['hous_id'])}}">{{$house['hous_number']}}</a></td>


    <?php if (!empty($house['tenants'])): ?>

        <?php foreach ($house['tenants'] as $t => $tenant): ?>


                    <?php if ( $tenant['tent_status'] == 1): ?>


                        <td>
                             <a href="{{route('tenants.show', $tenant['tent_id'])}}">
                                <?php
                                 echo ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname']);
                                ?>
                            </a> 
                        </td>


                    <?php endif ?>


        <?php endforeach ?>


<td><?php if ($house['hous_status'] == 1): ?>


                    Occupied
                

<?php else: ?>
    
    <a href="{{route('houses.show',$house['hous_id'])}}">check status</a>

<?php endif ?></td>

<td>{{$house['hous_price']}}</td>

    <?php else: ?>

        <td>n/a</td>

    <?php endif ?>


<td>
    <a href="{{route('houses.show',$house['hous_id'])}}">view</a> |
    <a href="#">options</a>
</td> 


                        </tr> 

                    <?php endforeach ?>

                <?php endif ?>


            <?php endforeach ?>  


        <?php else: ?>

            <tr><td colspan="8"><h4>No properties yet!</h4></td></tr>

        <?php endif ?>

      </tbody>
    </table>
  </div>
    <div class="cc clearfix" id="tenants">
    <h3>Tenants</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>fullname</th>
            <th>House #</th>
            <th>Deposit</th>
            <th>updated</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($compound['houses'])): ?>
          <?php foreach ($compound['houses']as $h => $house): ?>
              <?php if (!empty($house)): ?>
                  <?php foreach ($house['tenants'] as $t => $tenant): ?>

                    <tr>
                        <td>
                             <a href="{{route('tenants.show', $tenant['tent_id'])}}">
                                <?php
                                 echo ucwords($tenant['person']['pers_fname'].' '.$tenant['person']['pers_mname'].' '.$tenant['person']['pers_lname']);
                                ?>
                            </a> 
                        </td> 
                        <td><a href="{{route('houses.show',$house['hous_id'])}}">{{$house['hous_number']}}</a></td> 
                        <td>{{$tenant['tent_advance']}}</td> 
                        <td>{{$tenant['updated_at']}}</td> 
                        <td>{{$tenant['created_at']}}</td> 
                    </tr>                        
                  <?php endforeach ?>
              <?php endif ?>
                                            
            <?php endforeach ?>  
        <?php else: ?>
        <tr><td colspan="8"><h4>No house yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>
  <div class="cc clearfix" id="transactions">
    <h3>Payment Transactions</h3>
    <hr>
    <table class="table">
      <thead>
        <tr>
            <th>Payer</th>
            <th>House</th>
            <th>recorded by</th>
            <th>Amount</th>
            <th>For:</th>
            <th>Description</th>
            <th>updated</th>
            <th>created</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($customer['transactions'])): ?>
          <?php foreach ($customer['transactions'] as $key => $trans): ?>
            <tr>
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
                <td>data</td> 
            </tr>                                              
            <?php endforeach ?>  
        <?php else: ?>
         <tr><td colspan="8"><h4>No Payment yet!</h4></td></tr>
        <?php endif ?>
      </tbody>
    </table>
  </div>

</div>
@stop
@include('templates/bottom-admin')