<?php #page specific processing
$ll = Landlord::all();
	$agents = Agent::with('person')->get();
	$agents = (!empty($agents))? $agents->toArray() : [];

 ?>
@include('templates/top-admin')
@section('content')
@include('__partials/modal-edit-compound')
	<div class="c-header cc">
		<h3><?php echo (!empty($compound['name']))? ucwords($compound['name']): 'No compound name';  ?></h3>
		<hr>
		<div class="stats">
			<?php if (!empty($compound['landlord'])): ?>
				<a href="{{route('land-lords.show',$compound['landlord']['id'] )}}"><?php echo ($compound['landlord']['ll_fullname'] )? : 'No landlord';?></a> | 
			<?php endif ?>
			<?php if (!empty($compound['landlord'])): ?>
				<span>{{$compound['created_at']}}</span> |
				<span>{{$compound['updated_at']}}</span> |
				 <a href="#edit-compound" role="button" data-toggle="modal">Edit</a>		
			<?php endif ?>	
		</div>
	</div>
	<div class="cc">
		<table class="table">
			<thead>
				<tr>
					<th>House</th>
					<th>House price</th>
					<th>House availability</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($compound['houses'])): ?>
				<?php foreach ($compound['houses'] as $h => $house): ?>
					<tr>
						<td>{{$house['hous_number']}}</td>
						<td>{{$house['hous_price']}}</td>
						<td>{{$house['hous_availability']}}</td>
						<td><a href="{{route('houses.show',$house['hous_id'])}}">view</a></td>
					</tr>					
				<?php endforeach ?>
						
					<?php else: ?>
					<tr>
						<td colspan="7"><h4>No Houses Available!</h4></td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
@stop
@include('templates/bottom-admin')