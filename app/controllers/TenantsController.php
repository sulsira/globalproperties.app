<?php

class TenantsController extends \AdminController {

	/**
	 * Display a listing of the resource.
	 * GET /tenants
	 *
	 * @return Response
	 */
	public function index()
	{
		// $all = House::with('tenants')->get();
		$all = Tenant::with('house','person.contacts')->get();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.index')->with('tenants',$all);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tenants/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data =  array();
		$houses = Compound::with(array('houses'=>function($query){
			$query->where('hous_status','=',0);
		}))->get();
 		$house = ( !empty($houses) )? $houses->toArray() : [];
 		$data['compounds'] = $house;
		$this->layout->content = View::make('admin.Tenants.create')->with('data', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tenants
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$person = array();
		$staff = array();
		$address = array();
		$contact = array();
		$tent = array();
		$done = false;
		if ($input) :
			foreach ($input as $k => $table) {
				if (is_array($table)) {
					if ($k == 'person') {
						$V = new services\validators\Person($table);
						if($V->passes()){
							$person = Person::create(array(
							'pers_fname' => $table['fname'], 
							'pers_mname' => ($table['mname']) ?: null, 
							'pers_lname' => $table['lname'], 
							'pers_type' => 'Tenant', 
							'pers_DOB' => $table['dob'], 
							'pers_gender' => $table['Pers_Gender'], 
							'pers_nationality' => $table['Pers_Nationality'], 
							'pers_ethnicity' => $table['Pers_Ethnicity'], 
							'pers_NIN' => $table['nin_id'] 
							));
							if ($person->id) {
								$done = true;
							}
						}else{
							$errors = $V->errors;
							return Redirect::back()->withErrors($errors)->withInput();							
						}


					}
					if ($k == 'address') {
						$address = $table;
						if ($person->id) {
							$address = array_add($address, 'Addr_EntityID', $person->id);
							$address = array_add($address, 'Addr_EntityType', 'Person');
							$V = new services\validators\Address($table);
							if($V->passes()){
								$address = Address::create($address);
							}
						}
						$errors = $V->errors;

					}
					if ($k == 'contact') {

						

						if ($person->id) {

							$V = new services\validators\Contact($table);
							foreach ($table as $key => $value) {
								if($V->passes()){

									if(!empty($value)){
										$contact = Contact::create(array(
										'Cont_EntityID' => $person->id,	
										'Cont_EntityType' => 'Person',	
										'Cont_Contact' => $value,	
										'Cont_ContactType' =>  $key	
										));

									$contact = $contact->toArray();
									}
								}
							}
							$errors = $V->errors;
						}
					}

				}
			}
			if($done){

			if (!empty($input['houseID']) || !empty($input['Monthly_fee']) ) {


					$house = House::where('hous_id','=',$input['houseID'])->first();
					$house = (!empty($house))? $house->toArray() : [];
	
									
					if (!empty($house)) {
						$tent = Tenant::create(array(
								'tent_houseID'=> $input['houseID'],
								// 'tent_compoundID'=> $house['hous_compoundID'],
								'tent_advance'=> $input['down_payment'],
								'tent_personid'=> $person->id,
								'tent_status'=> 1
						));
					}else{

						$tent = Tenant::create(array(
								'tent_houseID'=> $input['houseID'],
								// 'tent_compoundID'=> $house['hous_compoundID'],
								'tent_advance'=> $input['down_payment'],
								'tent_personid'=> $person->id,
								'tent_status'=> 4 #does not have a house
						));	
									
					}

					// we add rent 
					$montlyfee = 0;   
					// dd($house);
					if( isset($input['Monthly_fee']) && !empty($input['Monthly_fee']) ):
						$montlyfee = $input['Monthly_fee'];
					else:
						if(!empty($house)):
		  					$montlyfee = $house['hous_price'] ;
		  				endif;
					endif;
					$ren = Rent::create(array(
						'rent_houseID' => $input['houseID'],
						'rent_tenantID' => $tent->tent_id,
						'rent_advance' => $input['down_payment'],
						'rent_firstmonthpaid' => null,
						'rent_nextpaydate' => null,
						'rent_monthlyFee' => $montlyfee
					));
					$house = House::find($input['houseID']);
					$tenanting = Tenant::find($tent->tent_id);
					if(!empty( $tenanting )):
						$tenanting->tent_rentID = $ren->rent_id;
						$tenanting->save();
					endif;

					if(!empty( $house )):
						$house->hous_status = 1;
						$house->save();
					endif;


	
			} #end of monthly fee and house


					if ($input['photo']) {


							$not = 'media'.DS.'Tenant'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
							$foldername = DS.$not.DS;
							// $full_dir = $foldername.'img'.DS.'full';

							$path = base_path().$foldername ;
							$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;
							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$filename = Input::file('photo')->getClientOriginalName();
							$shortened = base_convert(rand(10000,99999), 10, 36);
							$rename = $person->pers_fname.'_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;

							if ((File::exists($path)) && ( File::exists( $path.DS.'img'.DS ) ) && ( File::exists( $path.DS.'img'.DS.'thumbnail') ) ) {
								Input::file('photo')->move($path.DS.'img'.DS, $rename);
									#notice #01
										// there was a problem when trying to resize the image. the fix is go in the project dir (html but ! include)

									// we resize (250 X 300)
									$img = Image::make($not.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Person',
										'entity_ID' => $person->id,
										'type' => 'Photo',
										'fullpath' => $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $not,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
								
								# code...
							}else{
								File::exists($path) ?: mkdir($path,0777,true);
								File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS,0777,true);
								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail',0777,true);
								Input::file('photo')->move($path.DS.'img'.DS , $rename);
									// we resize (250 X 300)
									// dd($not.DS.$rename);
									$img = Image::make($not.DS.'img'.DS.$rename);
									$img->resize(250 , 300);
									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								Document::create(array(
										'title' => 'profile picture' ,
										'entity_type' => 'Person',
										'entity_ID' => $person->id,
										'type' => 'Photo',
										'fullpath' =>  $path.DS.'img'.DS.$rename,
										'filename' => $filename,
										'foldername' => $not,
										'extension' => $fileexten,
										'filetype' => $filetyp,
										'thumnaildir' => $thumbnail_dir.$rename
									));
							}
					} #end of photo








				Flash::message("Successfully added a Tenant");
				return Redirect::back();
			}else{
				return Redirect::back()->withErrors($errors)->withInput();							
			}
		endif;
	}

	/**
	 * Display the specified resource.
	 * GET /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		// dd($all);
		$this->layout->content = View::make('admin.Tenants.show')->with('tenant',$all);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tenants/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments','person.documents')->whereRaw('tent_id = ? AND deleted = ?',[$id,0])->first();
		//$all = Tenant::with('house.compound','person.contacts','person.addresses','rents.payments')->whereRaw('tent_id = ? AND deleted = ? AND tent_status = ?',[$id,0,0])->first();
		$all = ($all)? $all->toArray() : [];
		$this->layout->content = View::make('admin.Tenants.edit')->with('tenant',$all);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		
	$input = Input::all();
		if($input['type'] == 'person'):
			$person = Person::findOrFail($input['person_id']);
			$person->fill($input);
			$person->save();
			return Redirect::back();
		endif;

		if($input['type'] == 'address'):

			$person = Address::findOrFail( $input['address_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();	
		endif;

		if($input['type'] == 'contact'):

			$person = Contact::findOrFail( $input['contact_id'] );
			$person->fill($input);
			$person->save();
			return Redirect::back();

		endif;

		if($input['type'] == 'image'):

			$path = $path = base_path().DS.$input['foldername'] ; #notice 006 would fail if not set
			$not =  '';
			$foldername = '';
			$fn = '';
			if(starts_with($input['foldername'], '/')):

				$person = Document::find($input['image_id'])->person()->first();
				$not = 'media'.DS.'Tenant'.DS.$person->pers_fname.'_'.$person->pers_mname.'_'.$person->pers_lname.'_'.$person->id;
				$foldername = DS.$not.DS;
				$fn .= $not;

			else:

				$not = $input['foldername'];
				$foldername = DS.$not.DS;

			endif;	
			$thumbnail_dir = $not.DS.'img'.DS.'thumbnail'.DS;

			$fileexten = Input::file('photo')->getClientOriginalExtension();

			$filename = Input::file('photo')->getClientOriginalName();

			$shortened = base_convert(rand(10000,99999), 10, 36);

					if (File::exists($path)) {

					// 	// the folder exist then add a mew  picture and record in databse
					// 	// disable or delete the previous img
							

							


							$rename = 'renamed_'.$shortened.'_photo_'.$filename;
							File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true); #notice 007 a hack 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails

								// store in the database
								$document = Document::findOrFail( $input['image_id'] );
								$document->thumnaildir = $thumbnail_dir.$rename;
								$document->fullpath = $input['foldername'].'img'.DS.$rename;
								$document->filename = $filename ;
								if($fn):
									$document->foldername = $fn;
								endif;
								$document->extension = $fileexten;
								$document->save();

								return Redirect::back();	
															
					}else{

							// $full_dir = $foldername.'img'.DS.'full';

							$rename = 'newly_'.$shortened.'_photo_.'.$filename;


							$filetyp = Input::file('photo')->getMimeType();

							$fileexten = Input::file('photo')->getClientOriginalExtension();
							$uploaded = false;

							File::exists($path) ?: mkdir($path, 0755, true);

							 File::exists($path.DS.'img'.DS) ?: mkdir($path.DS.'img'.DS, 0755, true);

								File::exists($path.DS.'img'.DS.'thumbnail') ?: mkdir($path.DS.'img'.DS.'thumbnail', 0755, true);
								// var_dump($not.DS.'img'.DS , $rename);
								// die();
								
								Input::file('photo')->move($not.DS.'img'.DS , $rename);
									// we resize (250 X 300)
								 

								 Input::file('photo')->move($not.DS.'img'.DS , $rename);

									$img = Image::make($not.DS.'img'.DS.$rename);

									$img->resize(250 , 300);

									$img->save($thumbnail_dir.$rename);	// we move to thumnails



								$document = Document::findOrFail( $input['image_id'] );
								$document->thumnaildir = $thumbnail_dir.$rename;
								$document->fullpath = $input['foldername'].'img'.DS.$rename;
								$document->filename = $filename ;
								$document->foldername = $not;
								$document->extension = $fileexten;
								$document->save();


					}



		endif;	
		return Redirect::back();	

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tenants/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Tenant::destroy($id);
		return Redirect::back();
	}

}