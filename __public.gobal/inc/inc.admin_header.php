    <div class="container">
      
      <!-- start of logo, and nav -->
      <div class="logo">
        <div>
          <a href="<?php echo site_url('home') ?>"><img src="<?php echo base_url(); ?>__public/img/logo.png" class="logo" alt="MOHERST logo"></a>
          <p class="description">
             Moherst Information System
          </p>
        </div><!-- end of logo -->
      </div><!-- end of grid fluid -->
      
      <!-- start of main navigation -->
        <div class="navbar-wrapper">
        <div class="navbar navbar-inverse navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
              <li class="active selected"> <?php echo anchor('home','Home') ?></li>
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Sector <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li> <?php echo anchor('about_sector','About Sector') ?></li>
                <li> <?php echo anchor('admin/user/login','Login Sector') ?></li>
                <!--.dropdown-->
              </ul>
              </li>
             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Scholarship <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><?php echo anchor('scholarships','Scholarships') ?> </li>
                <li><?php echo anchor('how_to_apply','How to Apply') ?> </li>
                <li><?php echo anchor('apply_scholarship','Apply') ?> </li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tertiarry & Higher Edu <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><?php echo anchor('about_higher_edu','Tertiarry & Higher Edu') ?> </li>
                <li><?php echo anchor('learning_centers','Leraning Centers') ?> </li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Research & Development <b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><?php echo anchor('about_research','About Research') ?> </li>
                <li><?php echo anchor('register_research','Register') ?> </li>
                <li><?php echo anchor('researchers','Researchers') ?> </li>
                <li><?php echo anchor('publications','Publications') ?> </li>
                <!--.dropdown-->
              </ul>
              </li>

             
              <li><?php echo anchor('sci_tect_ino','Sci Tech & Innovation') ?></li>

              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Username<b class="caret"></b></a>
              <ul class="dropdown-menu js-activated">
                <li><?php echo anchor('about-research','About Research') ?></li>
                <li><?php echo anchor('register-research','Register') ?></li>
                <li><?php echo anchor('researchers','Researchers') ?></li>
                <li><?php echo anchor('publications','Publications') ?></li>
                <!--.dropdown-->
              </ul>
              </li>
            </ul>
            </div>
          </div>
        </div>
    </div>
      <!-- end of main navigation -->

    </div><!-- end of container -->